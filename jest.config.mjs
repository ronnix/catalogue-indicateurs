import nextJest from "next/jest.js";

const createBaseJestConfig = nextJest({
  // Provide the path to your Next.js app to load next.config.js and .env files in your test environment
  dir: "./src",
});

// Add any custom config to be passed to Jest
/** @type {import('jest').Config} */
const config = {
  testEnvironment: "jest-environment-jsdom",

  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "clipboard-polyfill": "<rootDir>/node_modules/clipboard-polyfill/dist/es6/clipboard-polyfill.es6.js",
  },
  testMatch: ["**/__tests__/**/?(*.)+(spec|test).[jt]s?(x)"],
};


// We are using a workaround to override transformIgnorePatterns
// as it is not possible to do it in the regular config
// @see https://github.com/vercel/next.js/issues/35634
const asyncConfig = createBaseJestConfig(config);

const jestConfig = async () => {
  const baseJestConfig = await asyncConfig();

  // mdast-util-toc is using esm imports/exports so we have to transform it
  baseJestConfig.transformIgnorePatterns = [
    "/node_modules/(?!clipboard-polyfill)/",
  ];

  return baseJestConfig;
};

// jestConfig is exported this way to ensure that next/jest can load the Next.js config which is async
export default jestConfig(config);
