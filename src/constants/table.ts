export const TABLE_CONFIG = {
  vacance_commerciale: {
    taux: "%",
  },
  logements_vacants: {
    taux: "%",
  },
  bataillons_prevention: {
    dotation_2021: "€",
    dotation_2022: "€",
  },
};

export const TABLE_HEADERS = ["Territoire", "Valeur", "Date de mise à jour"];
