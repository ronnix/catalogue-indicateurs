export const CATEGORIES = [
  { nom: "Politique de la ville", identifiant: "ville" as const },
  { nom: "Territoires et ruralités", identifiant: "territoires" as const },
  { nom: "Numérique", identifiant: "numerique" as const },
  { nom: "Appui opérationnel et stratégique", identifiant: "aos" as const },
  { nom: "Autres", identifiant: "autres" as const },
];
