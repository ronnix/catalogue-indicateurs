export const PROGRAMMES = [
  {
    identifiant: "crte",
    nom: "Contrats de relance et de transition écologique",
    categorie: "territoires",
  },
  {
    identifiant: "france_mobile",
    nom: "France mobile",
    categorie: "numerique",
  },
  {
    identifiant: "acv",
    nom: "Action cœur de ville",
    categorie: "territoires",
  },
  { identifiant: "insee", nom: "INSEE", categorie: "autres" },
  {
    identifiant: "france_services",
    nom: "France services",
    categorie: "territoires",
  },
  {
    identifiant: "frla",
    nom: "Fonds de restructuration des locaux d’activité",
    categorie: "aos",
  },
  {
    identifiant: "fthd",
    nom: "France très haut débit",
    categorie: "numerique",
  },
  {
    identifiant: "incubateur",
    nom: "Incubateur des territoires",
    categorie: "numerique",
  },
  {
    identifiant: "avenir_montagne",
    nom: "Avenir montagne",
    categorie: "territoires",
  },
  {
    identifiant: "pvd",
    nom: "Petites villes de demain",
    categorie: "territoires",
  },
  {
    identifiant: "nlnl",
    nom: "Nouveaux lieux, nouveaux liens ",
    categorie: "numerique",
  },
  {
    identifiant: "qpv",
    nom: "Quartiers prioritaires de la ville",
    categorie: "ville",
  },
  { identifiant: "asm", nom: "Accompagnement sur mesure", categorie: "aos" },
  {
    identifiant: "fabriques_prospectives",
    nom: "Fabriques prospectives",
    categorie: "aos",
  },
  { identifiant: "cdv", nom: "Contrats de ville", categorie: "ville" },
  {
    identifiant: "npnru",
    nom: "Nouveau programme national de renouvellement urbain",
    categorie: "ville",
  },
  {
    identifiant: "epe",
    nom: "Education et petite enfance",
    categorie: "ville",
  },
  {
    identifiant: "efde",
    nom: "Emploi, formation et développement économique",
    categorie: "ville",
  },
  {
    identifiant: "zrr",
    nom: "Zones de revitalisation rurale",
    categorie: "territoires",
  },
  {
    identifiant: "inclusion_numerique",
    nom: "Inclusion numérique",
    categorie: "numerique",
  },
  {
    identifiant: "hubs_territoriaux",
    nom: "Hubs territoriaux",
    categorie: "numerique",
  },
  {
    identifiant: "territoires_industrie",
    nom: "Territoires d’industrie",
    categorie: "territoires",
  },
  {
    identifiant: "villages_d_avenir",
    nom: "Villages d’avenir",
    categorie: "territoires",
  },
  {
    identifiant: "vta",
    nom: "Volontaires territoriaux en administration",
    categorie: "territoires",
  },
];
