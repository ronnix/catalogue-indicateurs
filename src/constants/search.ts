export const FUSE_OPTIONS = {
  keys: [
    { name: "nom", weight: 2 },
    { name: "tags.value", weight: 2 },
    "identifiant",
  ],
  threshold: 0.35,
};
