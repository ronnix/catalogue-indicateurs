export const MAX_BAR_THICKNESS = 80;

export const CHARTJS_OPTIONS = {
  responsive: true,
  animation: {},
  transitions: {
    show: {
      animations: {
        x: {
          from: 0,
        },
        y: {
          from: 0,
        },
      },
    },
    hide: {
      animations: {
        x: {
          to: 0,
        },
        y: {
          to: 0,
        },
      },
    },
  },
};
