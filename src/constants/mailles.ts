export const PAYS_OPTION = {
  value: "fr" as const,
  label: "National",
  maille: "pays" as const,
};

export const MAILLE_MAP = {
  region: "région",
  departement: "département",
  pays: "pays",
} as const;
