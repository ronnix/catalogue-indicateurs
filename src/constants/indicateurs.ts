export const PROGRAMME_TAG_NAME = "programme";

// This constant represents metrics identifiers that are no longer
// relevant, they are displaying the same data as their list
// counterpart with no added value. For now we are manually
// hiding them until they are properly deleted from our system.
export const DEPRECATED_METRICS = [
  "nb_crte",
  "nombre_villes_acv",
  "nombre_villes_anah",
  "nb_villes_laureates_au_coeur_des_territoires",
  "nb_antennes_formation_ouvertes",
  "nb_communes_terre_de_jeux_2024",
  "nb_laureates_tsf",
  "nb_territoires_cercle_tsf",
  "nb_villes_laureates_rcv",
  "nb_villes_accompagnees_rcv",
  "nombre_espaces_france_services",
  "nb_territoires_avenir_montagne_ingenierie",
  "nb_territoires_avenir_montagne_mobilites",
  "nb_territoires_avenir_montagne",
  "nb_communes_pvd",
  "nb_communes_zrr",
  "nb_communes_partiellement_zrr",
  "nb_territoires_industrie",
  "nb_collectivites_vta",
  "nb_collectivites_ansm",
  "nb_fabriques_territoire",
  "nb_manufactures_proximite",
  "nb_structures_accueil_conseillers_numeriques",
  "nb_hubs_territoriaux",
  "nombre_collectivites_fabriques_prospectives",
  "nb_qpv",
  "nb_qpv_2024",
  "nb_communes_qpv",
  "nb_communes_qpv_2024",
  "nb_fabriques_territoire_qpv",
  "nb_npnru",
  "nb_cites_educatives",
  "nb_communes_villages_d_avenir",
];

// This constant represents metrics identifiers we do not want to display
// in our lists, but for which we should still fetch data as their data
// is used to monkey patch data on other invalid metrics.
export const HIDDEN_METRICS = [
  "taux_logements_vacants",
  "taux_vacance_commerciale",
];
