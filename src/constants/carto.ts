import { formatNumericValue } from "@/lib/formatNumericValue";

export type Config = {
  path?: string;
  getLabel?: (
    entity: string | Record<string, string | number | null | undefined>,
  ) => string;
  preprocess?: (
    entity: string | Record<string, string | number | null | undefined>,
  ) => string;
};

export const CARTO_CONFIG: Record<string, Config> = {
  vacance_commerciale: {
    getLabel: (entity) =>
      typeof entity !== "string"
        ? `${entity.nccenr}: ${formatNumericValue({
            value: entity.taux,
            multiplier: 1,
          })}`
        : "",
    path: "nccenr",
  },
  logements_vacants: {
    getLabel: (entity) =>
      typeof entity !== "string"
        ? `${entity.nccenr}: ${formatNumericValue({
            value: entity.taux,
            multiplier: 1,
          })}`
        : "",
    path: "nccenr",
  },
  noms_villes_acv: {},
  noms_villes_anah: {},
  laureats_ami: {},
  noms_antennes_formation_ouvertes: {},
  noms_communes_terre_de_jeux_2024: {},
  laureates_tsf: {},
  territoires_cercle_tsf: {},
  noms_villes_laureates_rcv: {},
  noms_villes_accompagnees_rcv: {},
  noms_communes_pvd: {},
  noms_communes_zrr: {},
  noms_collectivites_vta: {},
  noms_collectivites_ansm: {},
  villes_demandeuses: {},
  liste_collectivites_fabriques_prospectives: {
    preprocess: (entity) =>
      typeof entity === "string" ? entity.slice(0, entity.indexOf("-")) : "",
  },
  noms_communes_contrats_de_ville: {},
  noms_communes_cites_educatives: {},
  noms_communes_zfu: {},
  noms_communes_qpv: {},
};
