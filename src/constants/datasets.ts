export const EXCLUDED_DATASETS = new Set([
  "collectivite",
  "commune",
  "departement",
  "epci",
  "groupements",
  "region",
]);
