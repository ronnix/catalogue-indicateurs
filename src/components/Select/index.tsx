"use client";

import React from "react";

import clsx from "clsx";
import { default as BaseSelect, GroupBase, Props } from "react-select";

import styles from "./styles.module.scss";

type SelectProps<
  Option = unknown,
  IsMulti extends boolean = false,
  Group extends GroupBase<Option> = GroupBase<Option>,
> = Props<Option, IsMulti, Group> & {
  label?: React.ReactNode;
  containerClassName?: string;
  labelClassName?: string;
};

const getInputId = (instanceId?: string | number) =>
  `react-select-${instanceId}-input`;

function Select<
  Option = unknown,
  IsMulti extends boolean = false,
  Group extends GroupBase<Option> = GroupBase<Option>,
>({
  containerClassName,
  labelClassName,
  classNames,
  label,
  instanceId,
  ...rest
}: SelectProps<Option, IsMulti, Group>) {
  const { control, placeholder, dropdownIndicator, menu, ...classNamesRest } =
    classNames || {};

  return (
    <div className={clsx("fr-select-group", containerClassName)}>
      {label && (
        <label
          htmlFor={getInputId(instanceId)}
          className={clsx("fr-label", labelClassName)}
        >
          {label}
        </label>
      )}
      <BaseSelect
        classNames={{
          control: (props) =>
            clsx(styles.control, control?.(props), {
              [styles.focused]: props.isFocused,
            }),
          valueContainer: () => styles["value-container"],
          singleValue: () => styles["single-value"],
          multiValue: () =>
            clsx("fr-tag fr-tag--sm fr-tag--dismiss", styles["multi-value"]),
          multiValueLabel: () => styles["multi-value-label"],
          multiValueRemove: () => styles["multi-value-remove"],
          dropdownIndicator: (props) =>
            clsx(styles.indicator, dropdownIndicator?.(props)),
          indicatorSeparator: () => styles.separator,
          placeholder: (props) =>
            clsx(styles.placeholder, placeholder?.(props)),
          menu: (props) => clsx(styles.menu, menu?.(props)),
          option: ({ isSelected, isFocused }) =>
            clsx(styles.option, { [styles.selected]: isSelected || isFocused }),
          ...classNamesRest,
        }}
        placeholder="Sélectionner une option"
        className={styles.select}
        noOptionsMessage={() => "Pas de résultats"}
        instanceId={instanceId}
        {...rest}
      />
    </div>
  );
}

export default Select;
