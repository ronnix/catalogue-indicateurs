"use client";

import React from "react";

import clsx from "clsx";

import skeletonStyles from "./skeleton.module.scss";
import commonStyles from "../common/styles.module.scss";

const SkeletonSelect = () => {
  return (
    <div className={clsx("fr-select-group")}>
      <div
        className={clsx(commonStyles["skeleton-element"], skeletonStyles.label)}
      />
      <div
        className={clsx(
          commonStyles["skeleton-element"],
          skeletonStyles.select,
        )}
      />
    </div>
  );
};

export default SkeletonSelect;
