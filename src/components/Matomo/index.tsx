"use client";

import { useEffect } from "react";

import { init, push } from "@socialgouv/matomo-next";
import { usePathname } from "next/navigation";

import { usePrevious } from "@/hooks/usePrevious";

const MATOMO_URL = process.env.NEXT_PUBLIC_MATOMO_URL || "";
const MATOMO_SITE_ID = process.env.NEXT_PUBLIC_MATOMO_SITE_ID || "";

const Matomo = () => {
  const pathname = usePathname();

  const previousPathname = usePrevious(pathname);

  useEffect(() => {
    if (process.env.NODE_ENV !== "production") {
      return;
    }

    init({
      url: MATOMO_URL,
      siteId: MATOMO_SITE_ID,
    });
  }, []);

  // This code should be handled by @socialgouv/matomo-next but it does not support
  // Next 13's app router yet. This workaround is largely copied from their source
  // code and from https://github.com/SocialGouv/mda
  useEffect(() => {
    if (!pathname) {
      return;
    }

    if (pathname === previousPathname) {
      return;
    }

    if (previousPathname) {
      push(["setReferrerUrl", `${previousPathname}`]);
    }
    push(["setCustomUrl", pathname]);
    push(["deleteCustomVariables", "page"]);

    setTimeout(() => {
      push(["setDocumentTitle", document.title]);
      push(["trackPageView"]);
    });
  }, [pathname, previousPathname]);

  return null;
};

export default Matomo;
