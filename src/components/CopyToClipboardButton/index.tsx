"use client";

import React, { RefObject, useEffect, useState } from "react";

import { Chart } from "chart.js";
import Link from "next/link";

import { copyChartToClipboard } from "@/lib/copy/copyChartToClipboard";
import { copyIndicateurToClipboard } from "@/lib/copy/copyIndicateurToClipboard";
import { copyMapToClipboard } from "@/lib/copy/copyMapToClipboard";
import { hasClipboardSupport } from "@/lib/copy/hasClipboardSupport";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { isSafari } from "@/lib/isSafari";
import { Metadata } from "@/types/indicateur";

import Tooltip from "../Tooltip";

type CopyToClipboardButtonProps = {
  data: IndicateurViewData[];
  definition: Metadata;
  view: string;
  chartRef?: RefObject<Chart<"bar" | "pie", (string | number)[]>>;
  mapRef: RefObject<HTMLDivElement | null>;
  className?: string;
};

const SUCCESS_INDICATOR_DURATION = 500;

const CopyToClipboardButton = ({
  data,
  definition,
  view,
  chartRef,
  mapRef,
  className,
}: CopyToClipboardButtonProps) => {
  const [showSuccess, setShowSuccess] = useState(false);

  useEffect(() => {
    if (showSuccess) {
      const id = setTimeout(
        () => setShowSuccess(false),
        SUCCESS_INDICATOR_DURATION,
      );

      return () => clearTimeout(id);
    }
  }, [showSuccess]);

  // Firefox does not support copying images to clipboard, even with polyfill
  // so we will just hide the button on this browser for now.
  const canCopyCharts = hasClipboardSupport();
  // Safari cannot copy leaflet maps to clipboard
  const canCopyMaps = hasClipboardSupport() && !isSafari();

  const isChart = ["bar", "pie"].includes(view);

  const isMap = view === "map";

  return (
    <Tooltip
      open={showSuccess}
      title="Copié"
      arrow
      // Disable regular hover / focus opening
      onOpen={() => {}}
      onClose={() => {}}
      placement="right"
    >
      <div className={className}>
        <Link
          className="fr-link fr-text--sm fr-link--icon-right ri-file-copy-line"
          href="#"
          onClick={(event: React.MouseEvent) => {
            event.preventDefault();

            if (!showSuccess) {
              setShowSuccess(true);
            }

            if (isChart && canCopyCharts && chartRef) {
              copyChartToClipboard(chartRef);

              return;
            }

            if (isMap && canCopyMaps && mapRef) {
              copyMapToClipboard(mapRef);

              return;
            }

            copyIndicateurToClipboard(data, definition.nom ?? "");
          }}
        >
          Copier
        </Link>
      </div>
    </Tooltip>
  );
};

export default CopyToClipboardButton;
