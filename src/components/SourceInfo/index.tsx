import React from "react";

import clsx from "clsx";
import Link from "next/link";

import { extractSource } from "@/lib/dataset/extractSource";
import { Source } from "@/types/dataset";
import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";

const SOURCE_LABEL_MAP = {
  [Source.GRIST]: "Grist",
  [Source.AIRTABLE]: "Airtable",
  [Source.METABASE]: "Metabase",
  [Source.DATA_GOUV]: "data.gouv.fr",
  [Source.MANUAL]: "Fichier",
};

type RatingProps = {
  source: Source;
};

const Rating = ({ source }: RatingProps) => {
  return (
    <span className="fr-pl-1w">
      <span className={clsx("ri-star-fill", styles.full)} />
      <span
        className={clsx("ri-star-fill", styles.full, {
          [styles.empty]: source === Source.MANUAL,
        })}
      />
      <span
        className={clsx("ri-star-fill", styles.full, {
          [styles.empty]: [
            Source.MANUAL,
            Source.GRIST,
            Source.AIRTABLE,
            Source.METABASE,
          ].includes(source),
        })}
      />
    </span>
  );
};

type InfoProps = {
  dataset: DatasetWithIndicateurs;
  source: Source;
};

const Info = ({ dataset, source }: InfoProps) => {
  if (source === Source.MANUAL) {
    return (
      <>
        <div>Nom du fichier : {dataset.pathPattern}</div>
        {dataset.source?.startsWith("http") && (
          <div className="fr-mt-2w">
            <Link
              href={dataset.source || ""}
              className="fr-link fr-link--icon-right fr-icon-download-line"
            >
              Télécharger les données [{dataset.fileFormat}]
            </Link>
          </div>
        )}
      </>
    );
  }

  if (source === Source.GRIST) {
    return <div>Nom du tableau : {dataset.pathPattern?.split(".csv")[0]}</div>;
  }

  if (source === Source.AIRTABLE) {
    return (
      <div className="fr-mt-2w">
        <Link
          href={dataset.source || ""}
          className="fr-link"
          target="_blank"
          rel="noreferrer"
        >
          Voir le tableau airtable
        </Link>
      </div>
    );
  }

  return (
    <div className="fr-mt-2w">
      <Link
        href={dataset.source || ""}
        className="fr-link fr-link--icon-right fr-icon-download-line"
      >
        Télécharger les données [{dataset.fileFormat}]
      </Link>
    </div>
  );
};

const getExcelLabel = (dataset: DatasetWithIndicateurs, source: Source) =>
  source === Source.MANUAL && dataset.fileFormat?.includes("xls")
    ? " excel"
    : "";

const getExtensionLabel = (dataset: DatasetWithIndicateurs, source: Source) =>
  source === Source.MANUAL ? ` (${dataset.fileFormat})` : "";

type SourceInfoProps = {
  dataset: DatasetWithIndicateurs;
};

const SourceInfo = ({ dataset }: SourceInfoProps) => {
  const source = extractSource(dataset);

  return (
    <div className="fr-mt-1w">
      {SOURCE_LABEL_MAP[source]} {getExcelLabel(dataset, source)}{" "}
      {getExtensionLabel(dataset, source)}
      <Rating source={source} /> <Info source={source} dataset={dataset} />
    </div>
  );
};

export default SourceInfo;
