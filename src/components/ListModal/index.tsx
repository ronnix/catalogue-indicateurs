"use client";

import React from "react";

import { createModal } from "@codegouvfr/react-dsfr/Modal";

import Table from "@/components/indicateurs/Table";
import { copyListToClipboard } from "@/lib/copy/copyIndicateurToClipboard";
import { Metadata, IndicateurCalcule } from "@/types/indicateur";

import styles from "./styles.module.scss";

type ListModalProps = {
  modal: ReturnType<typeof createModal>;
  definition?: Metadata;
  data: string[] | Record<string, unknown>[];
};

const ListModal = ({ modal, definition, data }: ListModalProps) => {
  const isStringList = typeof data[0] === "string";

  const isUniqueValueList =
    Object.keys(definition?.schema?.items?.properties ?? {}).length === 1;

  return (
    <modal.Component
      className={styles.modal}
      title={definition?.nom}
      size="large"
      buttons={[
        {
          children: "Copier la liste",
          doClosesModal: false,
          onClick: () => {
            copyListToClipboard(
              data,
              definition || ({ schema: {} } as Metadata),
            );
          },
        },
      ]}
    >
      <div className="fr-mb-2w">
        {isStringList || isUniqueValueList ? (
          <ul className={styles.list}>
            {data.map((item, index) => {
              if (typeof item === "string") {
                return (
                  <li className={styles["list-item"]} key={index}>
                    {item}
                  </li>
                );
              }

              const labelKey = Object.keys(
                definition?.schema?.items?.properties ?? {},
              )[0];

              return (
                <li className={styles["list-item"]} key={index}>
                  {item[labelKey] as string}
                </li>
              );
            })}
          </ul>
        ) : (
          <Table
            className="fr-table--layout-fixed"
            indicateur={
              {
                metadata: definition || { schema: {} },
                valeur: data,
              } as IndicateurCalcule
            }
          />
        )}
      </div>
    </modal.Component>
  );
};

export default ListModal;
