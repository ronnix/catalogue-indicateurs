import React, { ChangeEventHandler } from "react";

import clsx from "clsx";

import styles from "./styles.module.scss";

type CheckboxProps = {
  name: string;
  label: string;
  checked: boolean;
  onChange: ChangeEventHandler<HTMLInputElement>;
  small?: boolean;
  className?: string;
};

export default function Checkbox({
  name,
  checked,
  label,
  onChange,
  small,
  className,
}: CheckboxProps) {
  return (
    <div
      className={clsx("fr-checkbox-group", styles.container, className, {
        "fr-checkbox-group--sm": small,
      })}
    >
      <input
        type="checkbox"
        id={name}
        name={name}
        onChange={onChange}
        checked={checked}
      />
      <label className="fr-label" htmlFor={name}>
        {label}
      </label>
    </div>
  );
}
