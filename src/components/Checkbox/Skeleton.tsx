import React from "react";

import clsx from "clsx";

import skeletonStyles from "./skeleton.module.scss";
import styles from "./styles.module.scss";
import commonStyles from "../common/styles.module.scss";

export default function SkeletonCheckbox() {
  return (
    <div className={clsx("fr-checkbox-group", styles.container)}>
      <div
        className={clsx(commonStyles["skeleton-element"], skeletonStyles.label)}
      />
    </div>
  );
}
