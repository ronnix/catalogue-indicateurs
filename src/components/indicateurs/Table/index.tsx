import React from "react";

import Accordion from "@codegouvfr/react-dsfr/Accordion";

import BaseTable from "@/components/Table";
import { LIST_FALLBACK } from "@/constants/list";
import { TABLE_CONFIG } from "@/constants/table";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { pluralize } from "@/lib/pluralize";
import { removeEmpty } from "@/lib/removeEmpty";
import { extractTitledHeaders } from "@/lib/table/extractTitledHeaders";
import { IndicateurCalcule } from "@/types/indicateur";

import styles from "./styles.module.scss";

type TableProps = {
  indicateur: IndicateurCalcule;
  className?: string;
};

export const TableWithContainer = ({ indicateur }: TableProps) => {
  const label = pluralize(indicateur.metadata.nom, indicateur.valeur.length);

  return (
    <Accordion label={label}>
      <Table indicateur={indicateur} className="fr-table--layout-fixed" />
    </Accordion>
  );
};

export default function Table({ indicateur, className }: TableProps) {
  const { schema, unite } = indicateur.metadata;

  const columns = extractTitledHeaders(schema);

  const values = removeEmpty<Record<string, string | number>>(
    indicateur.valeur,
  );

  if (values.length === 0) {
    return LIST_FALLBACK;
  }

  return (
    <BaseTable
      className={className}
      headers={columns.map((column) => column.title)}
      data={values.map((value) =>
        columns.map((column) => value[column.key] || ""),
      )}
      renderCell={(item, index) => {
        const column = columns[index].key;
        const config =
          indicateur.metadata.identifiant in TABLE_CONFIG
            ? TABLE_CONFIG[
                indicateur.metadata.identifiant as keyof typeof TABLE_CONFIG
              ]
            : {};
        const unit = config[column as keyof typeof config] || unite;

        return (
          <td className={styles.cell} key={`${item}_${index}`}>
            {formatNumericValue({ value: item, unit, multiplier: 1 })}
          </td>
        );
      }}
    />
  );
}
