import Table from "@/components/Table";
import { extractTitledHeaders } from "@/lib/table/extractTitledHeaders";
import { DefinitionIndicateur } from "@/types/indicateur";

type TableSkeletonProps = {
  definition: DefinitionIndicateur;
  className?: string;
};

export default function TableSkeleton({
  definition,
  className,
}: TableSkeletonProps) {
  const columns = extractTitledHeaders(definition.schema);

  return (
    <Table
      className={className}
      headers={columns.map((column) => column.title)}
      data={Array.from(Array(6).keys()).map(() => columns.map(() => "-"))}
    />
  );
}
