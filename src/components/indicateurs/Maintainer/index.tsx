import Link from "next/link";

type MaintainerProps = {
  maintainer: string;
};

export default function Maintainer({ maintainer }: MaintainerProps) {
  const match = maintainer.match(/^(?<nom>.*) <(?<email>.*)>$/);
  if (!match) {
    return <span>{maintainer}</span>;
  }

  const { email, nom } = match.groups || {};

  return (
    <Link href={"mailto:" + email} className="fr-link">
      {nom}
    </Link>
  );
}
