import clsx from "clsx";
import Image from "next/image";
import Link from "next/link";

import featureRouter from "@/lib/features/router";

import HeaderAuth from "./Auth";
import Nav from "./Nav";
import styles from "./styles.module.scss";

const Links = () => {
  const authEnabled = featureRouter.auth();

  return (
    <ul className="fr-btns-group">
      <li>
        <a
          href="mailto:donnees@anct.gouv.fr"
          className="fr-btn fr-icon-mail-fill fr-text--sm"
        >
          Contactez-nous
        </a>
      </li>
      {authEnabled && <HeaderAuth />}
    </ul>
  );
};

export default function Header() {
  return (
    <>
      <header role="banner" id="fr-header" className="fr-header">
        <div className="fr-header__body">
          <div className="fr-container">
            <div className="fr-header__body-row">
              <div className="fr-header__brand fr-enlarge-link">
                <div className="fr-header__brand-top">
                  <div className="fr-header__logo">
                    <p className="fr-logo">
                      République
                      <br />
                      Française
                    </p>
                  </div>
                  <div className="fr-header__operator">
                    <Link
                      title="Accueil – Catalogue d’indicateurs ANCT"
                      href="/"
                      style={{ position: "relative" }}
                    >
                      <Image
                        src="/logos/logo_ANCT.svg"
                        alt="Logo ANCT"
                        className={clsx("fr-responsive-img", styles.logo)}
                        width={1}
                        height={1}
                      />
                    </Link>
                  </div>
                  <div className="fr-header__navbar">
                    <button
                      className="fr-btn--menu fr-btn"
                      data-fr-opened="false"
                      aria-controls="header-menu-modal-fr-header"
                      aria-haspopup="menu"
                      id="fr-header-menu-button"
                      title="Menu"
                      data-fr-js-modal-button="true"
                    >
                      Menu
                    </button>
                  </div>
                </div>
                <div className="fr-header__service">
                  <Link title="Accueil – Catalogue d’indicateurs ANCT" href="/">
                    <p className="fr-header__service-title">
                      Catalogue d’indicateurs
                      <span className="fr-badge fr-badge--new fr-badge--no-icon">
                        BETA
                      </span>
                    </p>
                  </Link>
                </div>
              </div>
              <div className="fr-header__tools">
                <div
                  className="fr-header__tools-links"
                  data-fr-js-header-links="true"
                >
                  <Links />
                </div>
              </div>
            </div>
          </div>
        </div>
        <div
          className="fr-header__menu fr-modal"
          id="header-menu-modal-fr-header"
          data-fr-js-modal="true"
          data-fr-js-header-modal="true"
          aria-labelledby="fr-header-menu-button"
        >
          <div className="fr-container">
            <button
              id="fr-header-mobile-overlay-button-close"
              className="fr-btn--close fr-btn"
              aria-controls="header-menu-modal-fr-header"
              title="Fermer"
              data-fr-js-modal-button="true"
            >
              Fermer
            </button>
            <div className="fr-header__menu-links">
              <Links />
            </div>
            <Nav />
          </div>
        </div>
      </header>
    </>
  );
}
