import React, { Suspense } from "react";

import { fetchDefinitions } from "@/lib/api/indicateurs";
import { fetchProgrammes, getProgrammesOptions } from "@/lib/api/programmes";

import Menu from "./Menu";

const Nav = async () => {
  const programmes = await fetchProgrammes();
  const definitions = await fetchDefinitions();
  const programmesOptions = getProgrammesOptions(programmes, definitions);

  return (
    <Suspense fallback={null}>
      <Menu programmes={programmesOptions} />
    </Suspense>
  );
};

export default Nav;
