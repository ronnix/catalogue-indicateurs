"use client";

import React from "react";

import clsx from "clsx";
import Link from "next/link";
import { usePathname, useSearchParams } from "next/navigation";

import { OptionGroup } from "@/types/select";

import styles from "./styles.module.scss";

type MenuProps = {
  programmes: OptionGroup[];
};

const Menu = ({ programmes }: MenuProps) => {
  const pathname = usePathname();
  const searchParams = useSearchParams();
  const territoires = searchParams.get("territoires");

  return (
    <nav
      id="fr-header-main-navigation"
      className="fr-nav"
      role="navigation"
      aria-label="Menu principal"
      data-fr-js-navigation="true"
    >
      <ul className="fr-nav__list">
        <li className="fr-nav__item" data-fr-js-navigation-item="true">
          <Link
            id="fr-header-main-navigation-link-0"
            className="fr-nav__link"
            href="/"
          >
            Accueil
          </Link>
        </li>
        <li className="fr-nav__item" data-fr-js-navigation-item="true">
          <button
            className="fr-nav__btn"
            aria-expanded="false"
            aria-controls="programmes"
            aria-current={pathname.includes("/indicateurs")}
          >
            Indicateurs
          </button>
          <div
            className="fr-collapse fr-mega-menu"
            id="programmes"
            tabIndex={-1}
          >
            <div className="fr-container fr-container--fluid fr-container-lg">
              <button
                className={clsx("fr-link--close fr-link", styles.close)}
                aria-controls="programmes"
              >
                Fermer
              </button>
              <div className="fr-grid-row fr-grid-row-lg--gutters">
                {programmes.map((group) => (
                  <div className="fr-col-12 fr-col-lg-3" key={group.label}>
                    <h5 className="fr-mega-menu__category fr-nav__link">
                      {group.label}
                    </h5>
                    <ul className="fr-mega-menu__list">
                      {group.options.map((programme) => (
                        <li key={programme.value}>
                          <Link
                            className="fr-nav__link"
                            href={`/indicateurs/${
                              programme.value
                            }?territoires=${territoires || ""}`}
                            target="_self"
                            {...(pathname.includes(programme.value)
                              ? { "aria-current": "page" }
                              : {})}
                          >
                            {programme.label}
                          </Link>
                        </li>
                      ))}
                    </ul>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </li>
        <li className="fr-nav__item" data-fr-js-navigation-item="true">
          <Link
            id="fr-header-main-navigation-link-1"
            className="fr-nav__link"
            href="/donnees"
            aria-current={pathname.includes("/donnees")}
          >
            Données
          </Link>
        </li>
      </ul>
    </nav>
  );
};

export default Menu;
