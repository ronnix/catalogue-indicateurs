"use client";

import React from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import Link from "next/link";
import { useSession, signOut } from "next-auth/react";

export default function HeaderAuth() {
  const { data: session } = useSession();

  return session ? (
    <li>
      <Button
        onClick={() => signOut()}
        className="fr-btn ri-logout-box-r-line fr-text--sm"
      >
        Se déconnecter
      </Button>
    </li>
  ) : (
    <li>
      <Link href="/login" className="fr-btn fr-icon-account-fill fr-text--sm">
        Se connecter
      </Link>
    </li>
  );
}
