import { Footer as DsfrFooter } from "@codegouvfr/react-dsfr/Footer";

import styles from "./styles.module.scss";
import DisplayModal, { FooterDisplayItem } from "../Display";

export default function Footer() {
  return (
    <>
      <DisplayModal />
      <DsfrFooter
        className={styles.footer}
        accessibility="non compliant"
        contentDescription={
          <>
            <a
              className="fr-link fr-text--sm"
              target="_blank"
              rel="noreferrer"
              href="https://donnees.incubateur.anct.gouv.fr/"
            >
              Données et Territoires
            </a>{" "}
            est une mission de{" "}
            <a
              className="fr-link fr-text--sm"
              target="_blank"
              rel="noreferrer"
              href="https://incubateur.anct.gouv.fr/"
            >
              l’Incubateur des Territoires
            </a>{" "}
            de{" "}
            <a
              className="fr-link fr-text--sm"
              target="_blank"
              rel="noreferrer"
              href="https://agence-cohesion-territoires.gouv.fr/"
            >
              l’Agence nationale de la cohésion des territoires
            </a>
            . Le{" "}
            <a
              className="fr-link fr-text--sm"
              target="_blank"
              rel="noreferrer"
              href="https://gitlab.com/incubateur-territoires/startups/donnees-et-territoires/catalogue-indicateurs"
            >
              code source
            </a>{" "}
            de ce site web est disponible sous une licence libre. Le design de
            ce site est conçu avec le{" "}
            <a
              className="fr-link fr-text--sm"
              target="_blank"
              rel="noreferrer"
              href="https://www.systeme-de-design.gouv.fr/"
            >
              système de design de l’État
            </a>
            .
          </>
        }
        operatorLogo={{
          alt: "Logo ANCT",
          imgUrl: "/logos/logo_ANCT.svg",
          orientation: "horizontal",
        }}
        brandTop={
          <>
            République
            <br />
            Française
          </>
        }
        homeLinkProps={{
          href: "/",
          title: "Accueil – Catalogue d’indicateurs ANCT",
        }}
        bottomItems={[
          <a
            className="fr-footer__bottom-link"
            href="/accessibilite"
            key="a11y"
          >
            Accessibilité&thinsp;: non conforme
          </a>,
          <FooterDisplayItem key="display" />,
        ]}
        license=""
      />
    </>
  );
}
