import React, { ChangeEvent, useMemo, useState } from "react";

import { SearchBar } from "@codegouvfr/react-dsfr/SearchBar";
import clsx from "clsx";

import { Option, OptionGroup } from "@/types/select";

import styles from "./styles.module.scss";

type OnChangeValue<Option, IsMulti extends boolean> = IsMulti extends true
  ? Option | null
  : Option[];

type MenuProps<OptionType extends Option, IsMulti extends boolean> = {
  id: string;
  label: React.ReactNode;
  options: (OptionGroup<OptionType> | OptionType)[];
  value: OptionType[];
  isMulti?: IsMulti;
  groupSelectionLabel?: React.ReactNode;
  onChange: (value: OnChangeValue<OptionType, IsMulti>) => void;
};

function Menu<
  OptionType extends Option = Option,
  IsMulti extends boolean = false,
>({
  id,
  options,
  label,
  value,
  onChange,
  groupSelectionLabel,
}: MenuProps<OptionType, IsMulti>) {
  const [searchTerm, setSearchTerm] = useState("");
  const selectAllLabel = groupSelectionLabel || "Tout sélectionner";

  const handleSearchTermChange = (event: ChangeEvent<HTMLInputElement>) => {
    setSearchTerm(event.target.value);
  };

  const filteredOptions = useMemo(() => {
    return options
      .map((option) => {
        if (!("options" in option)) {
          return option;
        }

        const { options, ...rest } = option;

        const childOptions = options.filter(
          (childOption) =>
            childOption.label.toLowerCase().includes(searchTerm) ||
            childOption.value.toLowerCase().includes(searchTerm),
        );

        return {
          ...rest,
          ...(childOptions.length > 0 ? { options: childOptions } : {}),
        };
      })
      .filter((option) => {
        const matches =
          option.label.toLowerCase().includes(searchTerm) ||
          option.value.toLowerCase().includes(searchTerm);

        return "options" in option ? true : matches;
      });
  }, [searchTerm, options]) as (OptionGroup<OptionType> | OptionType)[];

  return (
    <div className={clsx("fr-nav", styles.container)}>
      <ul className="fr-nav__list">
        <li className={clsx("fr-nav__item", styles.item)}>
          <button
            className={clsx("fr-select", styles.button)}
            aria-expanded="false"
            aria-controls={id}
          >
            {label}
          </button>
          <div
            className={clsx("fr-collapse fr-mega-menu fr-p-2w", styles.menu)}
            id={id}
          >
            <SearchBar
              className="fr-mb-2w"
              onButtonClick={() => {}}
              label="Rechercher"
              renderInput={({ className, id, placeholder, type }) => (
                <input
                  className={clsx(className)}
                  id={id}
                  placeholder={placeholder}
                  type={type}
                  value={searchTerm}
                  onChange={handleSearchTermChange}
                />
              )}
            />
            <div className="fr-accordions-group">
              {filteredOptions.map((groupOrOption) => {
                const selectable = groupOrOption.selectable !== false;
                const id = groupOrOption.value + groupOrOption.label;
                const selectAllId = "all-" + groupOrOption.value;
                const checked = value.some(
                  (entry) =>
                    entry.value === groupOrOption.value &&
                    entry.label === groupOrOption.label,
                );
                const handleChange = (
                  e: React.ChangeEvent<HTMLInputElement>,
                ) => {
                  const { options, ...rest } = groupOrOption as OptionGroup;
                  const updatedValues = (
                    e.target.checked
                      ? [...value, rest]
                      : value.filter(
                          (entry) =>
                            entry.label !== groupOrOption.label ||
                            entry.value !== groupOrOption.value,
                        )
                  ) as OnChangeValue<OptionType, IsMulti>;

                  onChange(updatedValues);
                };

                if (!("options" in groupOrOption)) {
                  return (
                    <section className="fr-accordion" key={id}>
                      <div
                        className={clsx(
                          "fr-checkbox-group fr-checkbox-group--sm",
                          styles.group,
                        )}
                      >
                        <input
                          name={id}
                          id={id}
                          type="checkbox"
                          checked={checked}
                          onChange={handleChange}
                        />
                        <label
                          className={clsx("fr-label", styles.label)}
                          htmlFor={id}
                        >
                          {groupOrOption.label}
                        </label>
                      </div>
                    </section>
                  );
                }

                const { selectedChildren, selectAllChecked } =
                  groupOrOption.options.reduce(
                    (acc, option) => {
                      const selected = value.some(
                        (entry) =>
                          entry.label === option.label &&
                          entry.value === option.value,
                      );
                      return {
                        selectedChildren: selected
                          ? acc.selectedChildren + 1
                          : acc.selectedChildren,
                        selectAllChecked: acc.selectAllChecked && selected,
                      };
                    },
                    {
                      selectedChildren: 0,
                      selectAllChecked: true,
                    },
                  );

                return (
                  <section className="fr-accordion" key={id}>
                    <div className="fr-accordion__title">
                      <div
                        className={clsx(
                          "fr-checkbox-group fr-checkbox-group--sm",
                          styles.group,
                        )}
                      >
                        {selectable ? (
                          <>
                            <input
                              name={id}
                              id={id}
                              type="checkbox"
                              checked={checked}
                              onChange={handleChange}
                            />
                            <label
                              className={clsx("fr-label", styles.label)}
                              htmlFor={id}
                            >
                              {groupOrOption.label}
                              {selectedChildren ? (
                                <span className="fr-text--bold">
                                  &nbsp;({selectedChildren})
                                </span>
                              ) : (
                                ""
                              )}
                            </label>
                          </>
                        ) : (
                          <div
                            className={clsx("fr-label fr-ml-3w", styles.label)}
                          >
                            {groupOrOption.label}{" "}
                            {selectedChildren ? (
                              <span className="fr-text--bold">
                                &nbsp;({selectedChildren})
                              </span>
                            ) : (
                              ""
                            )}
                          </div>
                        )}
                        <button
                          className={clsx(
                            "fr-accordion__btn",
                            styles["accordion-button"],
                          )}
                          aria-expanded="false"
                          aria-controls={`accordion-${id}`}
                        />
                      </div>
                    </div>
                    <div
                      className={clsx("fr-collapse", styles.container)}
                      id={`accordion-${id}`}
                    >
                      <div
                        className={clsx(
                          "fr-checkbox-group fr-checkbox-group--sm",
                          styles.group,
                          styles.option,
                        )}
                      >
                        <input
                          name={selectAllId}
                          id={selectAllId}
                          type="checkbox"
                          onChange={(e) => {
                            const valuesWithoutDepts = value.filter(
                              (entry) =>
                                !groupOrOption.options.some(
                                  (option) =>
                                    option.value === entry.value &&
                                    option.label === entry.label,
                                ),
                            );

                            const updatedValues = (
                              e.target.checked
                                ? [
                                    ...valuesWithoutDepts,
                                    ...groupOrOption.options,
                                  ]
                                : valuesWithoutDepts
                            ) as OnChangeValue<OptionType, IsMulti>;

                            onChange(updatedValues);
                          }}
                          checked={selectAllChecked}
                        />
                        <label
                          className={clsx("fr-label", styles.label)}
                          htmlFor={selectAllId}
                        >
                          {selectAllLabel}
                        </label>
                      </div>
                      <div className={styles.content}>
                        {groupOrOption.options.map((option) => {
                          const optionId = option.value + option.label;
                          const checked = value.some(
                            (entry) =>
                              entry.value === option.value &&
                              entry.label === option.label,
                          );

                          return (
                            <div
                              className={clsx(
                                "fr-checkbox-group fr-checkbox-group--sm",
                                styles.group,
                                styles.option,
                              )}
                              key={optionId}
                            >
                              <input
                                name={optionId}
                                id={optionId}
                                type="checkbox"
                                checked={checked}
                                onChange={(e) => {
                                  const updatedValues = (
                                    e.target.checked
                                      ? [...value, option]
                                      : value.filter(
                                          (entry) =>
                                            entry.label !== option.label ||
                                            entry.value !== option.value,
                                        )
                                  ) as OnChangeValue<OptionType, IsMulti>;

                                  onChange(updatedValues);
                                }}
                              />
                              <label
                                className={clsx("fr-label", styles.label)}
                                htmlFor={optionId}
                              >
                                {option.label}
                              </label>
                            </div>
                          );
                        })}
                      </div>
                    </div>
                  </section>
                );
              })}
            </div>
          </div>
        </li>
      </ul>
    </div>
  );
}

export default Menu;
