import React from "react";

import { UseQueryResult } from "@tanstack/react-query";
import clsx from "clsx";
import Link from "next/link";

import { NetworkError } from "@/lib/api/NetworkError";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";
import CopyAllToClipboardButton from "../CopyAllToClipboardButton";
import CopyLinkToClipboardButton from "../CopyLinkToClipboardButton";

type PageTitleProps = {
  className?: string;
  title: string;
  searchTerm?: string;
  definitions: Metadata[];
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[];
};

const PageTitle = ({
  className,
  title,
  searchTerm,
  definitions,
  data,
}: PageTitleProps) => {
  return (
    <section className={clsx("fr-pb-4w", styles.title, className)}>
      <div>
        <h3 className="fr-mb-0">{title}</h3>
        <Link className="fr-link fr-text--sm" href="/donnees">
          Voir toutes les sources de données
        </Link>
      </div>
      <div className={clsx(styles.actions)}>
        <CopyAllToClipboardButton definitions={definitions} data={data} />
        <CopyLinkToClipboardButton searchTerm={searchTerm || ""} />
      </div>
    </section>
  );
};

export default PageTitle;
