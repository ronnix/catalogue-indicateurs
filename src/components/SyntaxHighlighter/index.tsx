"use client";

import { PrismLight as BaseSyntaxHighlighter } from "react-syntax-highlighter";
import json from "react-syntax-highlighter/dist/esm/languages/prism/json";
import sql from "react-syntax-highlighter/dist/esm/languages/prism/sql";
import prism from "react-syntax-highlighter/dist/esm/styles/prism/prism";

BaseSyntaxHighlighter.registerLanguage("sql", sql);
BaseSyntaxHighlighter.registerLanguage("json", json);

type SyntaxHighlighterProps = {
  children: string;
  language: "sql" | "json";
};

const SyntaxHighlighter = ({ children, language }: SyntaxHighlighterProps) => {
  return (
    <BaseSyntaxHighlighter language={language} style={prism}>
      {children}
    </BaseSyntaxHighlighter>
  );
};

export default SyntaxHighlighter;
