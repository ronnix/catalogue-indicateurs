"use client";

import React, { useEffect, useState } from "react";

import Link from "next/link";
import { useSearchParams } from "next/navigation";

import { copyLinkToClipboard } from "@/lib/copy/copyLinkToClipboard";

import Tooltip from "../Tooltip";

type CopyLinkToClipboardButtonProps = {
  searchTerm: string;
};

const SUCCESS_INDICATOR_DURATION = 500;

const CopyLinkToClipboardButton = ({
  searchTerm,
}: CopyLinkToClipboardButtonProps) => {
  const [showSuccess, setShowSuccess] = useState(false);

  const searchParams = useSearchParams();

  useEffect(() => {
    if (showSuccess) {
      const id = setTimeout(
        () => setShowSuccess(false),
        SUCCESS_INDICATOR_DURATION,
      );

      return () => clearTimeout(id);
    }
  }, [showSuccess]);

  return (
    <Tooltip
      open={showSuccess}
      title="Copié"
      arrow
      // Disable regular hover / focus opening
      onOpen={() => {}}
      onClose={() => {}}
      placement="right"
    >
      <div>
        <Link
          className="fr-link fr-text--sm fr-link--icon-right ri-links-line"
          href="#"
          onClick={() => {
            copyLinkToClipboard(searchTerm, searchParams);

            if (!showSuccess) {
              setShowSuccess(true);
            }
          }}
        >
          Partager cette page
        </Link>
      </div>
    </Tooltip>
  );
};

export default CopyLinkToClipboardButton;
