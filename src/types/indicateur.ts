import {
  IndicateurCalcule as BaseIndicateurCalcule,
  DefinitionIndicateur as BaseDefinitionIndicateur,
  Metadata as BaseMetadata,
  Tag,
} from "./schema";

export type AnyOf = {
  anyOf: { type: "null" | "number" | "integer" }[];
  type?: never;
  title?: string;
  items?: never;
};

export type Property = {
  type: "string" | "integer" | "number" | "object";
  title?: string;
  anyOf?: never;
};

export type IndicateurSchema =
  | AnyOf
  | {
      type: "integer" | "number" | ("number" | "null" | "integer")[];
      anyOf?: never;
      items?: never;
    }
  | {
      type: "array";
      anyOf?: never;
      items:
        | {
            type: "string";
            properties?: never;
          }
        | {
            type: "object";
            properties?: {
              [key: string]: AnyOf | Property;
            };
          };
    };

type ExtendedTag = Tag & { nom?: string };

export type Metadata = Omit<BaseMetadata, "schema" | "identifiant" | "tags"> & {
  schema: IndicateurSchema;
  identifiant: string;
  tags: ExtendedTag[];
};

export type DefinitionIndicateur = Omit<
  BaseDefinitionIndicateur,
  "metadata"
> & {
  metadata: Metadata;
};

export type IndicateurCalcule = Omit<BaseIndicateurCalcule, "metadata"> & {
  metadata: Metadata;
};

export type ListData = string | string[] | Record<string, unknown>[] | null;
