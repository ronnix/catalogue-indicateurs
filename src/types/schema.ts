/* eslint-disable */
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
export type MakeEmpty<
  T extends { [key: string]: unknown },
  K extends keyof T,
> = { [_ in K]?: never };
export type Incremental<T> =
  | T
  | {
      [P in keyof T]?: P extends " $fragmentName" | "__typename" ? T[P] : never;
    };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string };
  String: { input: string; output: string };
  Boolean: { input: boolean; output: boolean };
  Int: { input: number; output: number };
  Float: { input: number; output: number };
  DICT: { input: any; output: any };
  Date: { input: any; output: any };
  DateTime: { input: any; output: any };
  JSON: { input: any; output: any };
};

export type Categorie = {
  __typename?: "Categorie";
  description?: Maybe<Scalars["String"]["output"]>;
  titre: Scalars["String"]["output"];
  valeur: Scalars["JSON"]["output"];
};

export type Change = {
  __typename?: "Change";
  current: IndicateurCalcule;
  hasChanged: Scalars["Boolean"]["output"];
  preview: IndicateurCalcule;
};

export type Dataset = {
  __typename?: "Dataset";
  description?: Maybe<Scalars["String"]["output"]>;
  fileFormat?: Maybe<Scalars["String"]["output"]>;
  lastImport?: Maybe<Scalars["DateTime"]["output"]>;
  lastModified?: Maybe<Scalars["Date"]["output"]>;
  maintainer?: Maybe<Scalars["String"]["output"]>;
  name: Scalars["String"]["output"];
  needsUpdate?: Maybe<Scalars["Boolean"]["output"]>;
  nextExpectedUpdate?: Maybe<Scalars["Date"]["output"]>;
  pathPattern?: Maybe<Scalars["String"]["output"]>;
  producer?: Maybe<Organisation>;
  source?: Maybe<Scalars["String"]["output"]>;
  table: Scalars["String"]["output"];
  updateFrequency?: Maybe<Scalars["String"]["output"]>;
};

export type DatasetWithIndicateurs = {
  __typename?: "DatasetWithIndicateurs";
  description?: Maybe<Scalars["String"]["output"]>;
  fileFormat?: Maybe<Scalars["String"]["output"]>;
  indicateurs: Array<DefinitionIndicateur>;
  lastImport?: Maybe<Scalars["DateTime"]["output"]>;
  lastModified?: Maybe<Scalars["Date"]["output"]>;
  maintainer?: Maybe<Scalars["String"]["output"]>;
  name: Scalars["String"]["output"];
  needsUpdate?: Maybe<Scalars["Boolean"]["output"]>;
  nextExpectedUpdate?: Maybe<Scalars["Date"]["output"]>;
  pathPattern?: Maybe<Scalars["String"]["output"]>;
  producer?: Maybe<Organisation>;
  source?: Maybe<Scalars["String"]["output"]>;
  table: Scalars["String"]["output"];
  updateFrequency?: Maybe<Scalars["String"]["output"]>;
};

export type DefinitionCategorie = {
  __typename?: "DefinitionCategorie";
  description?: Maybe<Scalars["String"]["output"]>;
  titre?: Maybe<Scalars["String"]["output"]>;
};

export type DefinitionDimension = {
  __typename?: "DefinitionDimension";
  categories: Array<DefinitionCategorie>;
  description?: Maybe<Scalars["String"]["output"]>;
  identifiant: Scalars["String"]["output"];
  titre?: Maybe<Scalars["String"]["output"]>;
};

export type DefinitionIndicateur = {
  __typename?: "DefinitionIndicateur";
  categorie?: Maybe<Scalars["String"]["output"]>;
  datasets: Array<Dataset>;
  description?: Maybe<Scalars["String"]["output"]>;
  dimensions: Array<DefinitionDimension>;
  identifiant: Scalars["String"]["output"];
  mailles: MailleIndicateur;
  metadata: Metadata;
  nom?: Maybe<Scalars["String"]["output"]>;
  recette: Scalars["JSON"]["output"];
  schema?: Maybe<Scalars["JSON"]["output"]>;
  tags: Array<Tag>;
  unite?: Maybe<Scalars["String"]["output"]>;
};

export type Dimension = {
  __typename?: "Dimension";
  categories: Array<Categorie>;
  description?: Maybe<Scalars["String"]["output"]>;
  identifiant: Scalars["String"]["output"];
  titre?: Maybe<Scalars["String"]["output"]>;
};

export type FiltreTag = {
  name: Scalars["String"]["input"];
  value: Scalars["String"]["input"];
};

export type GeoJsonGeometryInterface = {
  coordinates: Scalars["JSON"]["output"];
  type: Scalars["String"]["output"];
};

export type GeoJsonGeometryPoint = GeoJsonGeometryInterface & {
  __typename?: "GeoJSONGeometryPoint";
  coordinates: Scalars["JSON"]["output"];
  type: Scalars["String"]["output"];
};

export type GeoJsonGeometryPointGeoJsonFeatureCollection = {
  __typename?: "GeoJSONGeometryPointGeoJSONFeatureCollection";
  features: Array<GeoJsonGeometryPointGeoJsonFeatureType>;
  type: Scalars["String"]["output"];
};

export type GeoJsonGeometryPointGeoJsonFeatureType = {
  __typename?: "GeoJSONGeometryPointGeoJSONFeatureType";
  geometry: GeoJsonGeometryPoint;
  properties: Scalars["DICT"]["output"];
  type: Scalars["String"]["output"];
};

export type GeoJsonGeomtryMultiPolygon = GeoJsonGeometryInterface & {
  __typename?: "GeoJSONGeomtryMultiPolygon";
  coordinates: Scalars["JSON"]["output"];
  type: Scalars["String"]["output"];
};

export type GeoJsonGeomtryMultiPolygonGeoJsonFeatureCollection = {
  __typename?: "GeoJSONGeomtryMultiPolygonGeoJSONFeatureCollection";
  features: Array<GeoJsonGeomtryMultiPolygonGeoJsonFeatureType>;
  type: Scalars["String"]["output"];
};

export type GeoJsonGeomtryMultiPolygonGeoJsonFeatureType = {
  __typename?: "GeoJSONGeomtryMultiPolygonGeoJSONFeatureType";
  geometry: GeoJsonGeomtryMultiPolygon;
  properties: Scalars["DICT"]["output"];
  type: Scalars["String"]["output"];
};

export type IdentifiantInconnuError = {
  __typename?: "IdentifiantInconnuError";
  identifiant: Scalars["String"]["output"];
};

export type IndicateurCalcule = {
  __typename?: "IndicateurCalcule";
  code: Scalars["String"]["output"];
  dimensions: Array<Dimension>;
  metadata: DefinitionIndicateur;
  ratiosMaillesSuperieures: Scalars["JSON"]["output"];
  valeur?: Maybe<Scalars["JSON"]["output"]>;
  valeursMaillesSuperieures: Scalars["JSON"]["output"];
};

export type IndicateurCalculeResponse =
  | IdentifiantInconnuError
  | IndicateurCalcule;

export type IndicateurCalculeResponsePaginationWindow = {
  __typename?: "IndicateurCalculeResponsePaginationWindow";
  /** The list of items in this pagination window. */
  items: Array<IndicateurCalculeResponse>;
  /** Total number of items in the filtered dataset. */
  totalItemsCount: Scalars["Int"]["output"];
};

export type IndicateurListe = IndicateurTyped & {
  __typename?: "IndicateurListe";
  code: Scalars["String"]["output"];
  count: Scalars["Int"]["output"];
  geo?: Maybe<IndicateurListeGeo>;
  liste: Array<Scalars["JSON"]["output"]>;
  maille: Scalars["String"]["output"];
  mailleSuperieure: TypeIndicateur;
};

export type IndicateurListeGeo = IndicateurTyped & {
  __typename?: "IndicateurListeGeo";
  code: Scalars["String"]["output"];
  contours?: Maybe<GeoJsonGeomtryMultiPolygonGeoJsonFeatureCollection>;
  count: Scalars["Int"]["output"];
  geo?: Maybe<IndicateurListeGeo>;
  maille: Scalars["String"]["output"];
  mailleSuperieure: TypeIndicateur;
  points?: Maybe<GeoJsonGeometryPointGeoJsonFeatureCollection>;
  properties: Array<Scalars["DICT"]["output"]>;
};

export type IndicateurOneValue = IndicateurTyped & {
  __typename?: "IndicateurOneValue";
  code: Scalars["String"]["output"];
  dimensions: Array<Dimension>;
  geo?: Maybe<IndicateurListeGeo>;
  maille: Scalars["String"]["output"];
  mailleSuperieure: TypeIndicateur;
  valeur?: Maybe<Scalars["JSON"]["output"]>;
};

export type IndicateurPreview = {
  __typename?: "IndicateurPreview";
  communes?: Maybe<Array<TerritoirePreview>>;
  departements?: Maybe<Array<TerritoirePreview>>;
  hasChanged: Scalars["Boolean"]["output"];
  metadata: DefinitionIndicateur;
  pays?: Maybe<TerritoirePreview>;
  regions?: Maybe<Array<TerritoirePreview>>;
};

export type IndicateurRow = IndicateurTyped & {
  __typename?: "IndicateurRow";
  code: Scalars["String"]["output"];
  geo?: Maybe<IndicateurListeGeo>;
  maille: Scalars["String"]["output"];
  mailleSuperieure: TypeIndicateur;
  row: Scalars["DICT"]["output"];
};

export type IndicateurRows = IndicateurTyped & {
  __typename?: "IndicateurRows";
  code: Scalars["String"]["output"];
  count: Scalars["Int"]["output"];
  geo?: Maybe<IndicateurListeGeo>;
  maille: Scalars["String"]["output"];
  mailleSuperieure: TypeIndicateur;
  rows: Array<Scalars["DICT"]["output"]>;
};

export type IndicateurTyped = {
  code: Scalars["String"]["output"];
  geo?: Maybe<IndicateurListeGeo>;
  maille: Scalars["String"]["output"];
  mailleSuperieure: TypeIndicateur;
};

export type IndicateurValeurFiltre = {
  identifiants?: InputMaybe<Array<Scalars["String"]["input"]>>;
  showDeprecated?: Scalars["Boolean"]["input"];
  tags?: InputMaybe<Array<FiltreTag>>;
};

export type Indicateurs = {
  __typename?: "Indicateurs";
  code: Scalars["String"]["output"];
  indicateur: IndicateurCalcule;
  maille: Scalars["String"]["output"];
};

export type IndicateursIndicateurArgs = {
  identifiant?: InputMaybe<Scalars["String"]["input"]>;
  namespace?: InputMaybe<Scalars["String"]["input"]>;
};

export type Maille = {
  __typename?: "Maille";
  code: Scalars["String"]["output"];
  indicateurs: Indicateurs;
  libelle: Scalars["String"]["output"];
  maille: Scalars["String"]["output"];
};

export type MailleIndicateur = {
  __typename?: "MailleIndicateur";
  commune: TypeIndicateur;
  departement: TypeIndicateur;
  pays: TypeIndicateur;
  region: TypeIndicateur;
};

export type MailleIndicateurCommuneArgs = {
  code: Scalars["String"]["input"];
};

export type MailleIndicateurDepartementArgs = {
  code: Scalars["String"]["input"];
};

export type MailleIndicateurRegionArgs = {
  code: Scalars["String"]["input"];
};

export type Mailles = {
  __typename?: "Mailles";
  departements: Array<Maille>;
  pays: Array<Maille>;
  regions: Array<Maille>;
};

export type Metadata = {
  __typename?: "Metadata";
  categorie?: Maybe<Scalars["String"]["output"]>;
  datasets: Array<Dataset>;
  description?: Maybe<Scalars["String"]["output"]>;
  dimensions: Array<DefinitionDimension>;
  identifiant: Scalars["String"]["output"];
  mailles: Array<Scalars["String"]["output"]>;
  nom?: Maybe<Scalars["String"]["output"]>;
  recette: Scalars["JSON"]["output"];
  schema?: Maybe<Scalars["JSON"]["output"]>;
  tags: Array<Tag>;
  unite?: Maybe<Scalars["String"]["output"]>;
};

export type Organisation = {
  __typename?: "Organisation";
  name?: Maybe<Scalars["String"]["output"]>;
  url?: Maybe<Scalars["String"]["output"]>;
};

export type Query = {
  __typename?: "Query";
  commune: Maille;
  datasets: Array<DatasetWithIndicateurs>;
  departement: Maille;
  indicateurPreview: IndicateurPreview;
  indicateurs: Array<DefinitionIndicateur>;
  indicateursValeurs: IndicateurCalculeResponsePaginationWindow;
  mailles: Mailles;
  region: Maille;
};

export type QueryCommuneArgs = {
  code: Scalars["ID"]["input"];
};

export type QueryDatasetsArgs = {
  filename?: InputMaybe<Scalars["String"]["input"]>;
};

export type QueryDepartementArgs = {
  code: Scalars["ID"]["input"];
};

export type QueryIndicateurPreviewArgs = {
  identifiant: Scalars["String"]["input"];
  importIds: Array<Scalars["Int"]["input"]>;
};

export type QueryIndicateursArgs = {
  filtre?: InputMaybe<IndicateurValeurFiltre>;
  preview?: InputMaybe<QueryIndicateurPreview>;
};

export type QueryIndicateursValeursArgs = {
  code: Scalars["String"]["input"];
  filtre?: InputMaybe<IndicateurValeurFiltre>;
  limit?: InputMaybe<Scalars["Int"]["input"]>;
  maille: Scalars["String"]["input"];
  offset?: Scalars["Int"]["input"];
};

export type QueryRegionArgs = {
  code: Scalars["ID"]["input"];
};

export type QueryIndicateurPreview = {
  mailleOrigine?: InputMaybe<Scalars["String"]["input"]>;
  returnType: ReturnTypeEnum;
  sql: Scalars["String"]["input"];
};

export enum ReturnTypeEnum {
  List = "list",
  OneValue = "one_value",
  Row = "row",
  Rows = "rows",
}

export type Tag = {
  __typename?: "Tag";
  name: Scalars["String"]["output"];
  value: Scalars["String"]["output"];
};

export type TerritoirePreview = {
  __typename?: "TerritoirePreview";
  change: Change;
  code: Scalars["String"]["output"];
  libelle: Scalars["String"]["output"];
  maille: Scalars["String"]["output"];
};

export type TypeIndicateur =
  | IndicateurListe
  | IndicateurListeGeo
  | IndicateurOneValue
  | IndicateurRow
  | IndicateurRows;
