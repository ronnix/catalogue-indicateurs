export type TerritoireEntry = {
  entries: string[];
  code: string;
  label: string;
};
