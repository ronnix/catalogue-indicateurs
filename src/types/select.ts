export type Option = { value: string; label: string; selectable?: boolean };

export type TerritoireOption = Option & {
  maille: "region" | "departement" | "pays";
};

export type OptionGroup<T extends Option = Option> = T & {
  options: T[];
};

export type TerritoiresOptions = (
  | TerritoireOption
  | OptionGroup<TerritoireOption>
)[];
