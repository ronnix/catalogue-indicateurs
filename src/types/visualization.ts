import zod from "zod";

export const VisualizationSchema = zod.enum([
  "table",
  "pie",
  "bar",
  "map",
  "list",
] as const);

export type Visualization = zod.infer<typeof VisualizationSchema>;
