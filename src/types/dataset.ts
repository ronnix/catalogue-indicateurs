export enum Source {
  DATA_GOUV = "data.gouv",
  GRIST = "grist",
  AIRTABLE = "airtable",
  MANUAL = "manual",
  METABASE = "metabase",
}
