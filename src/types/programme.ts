export type Programme = {
  identifiant: string;
  nom: string;
  categorie: string;
};
