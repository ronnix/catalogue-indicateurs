"use client";

import React, { createContext, useContext } from "react";

import { TerritoiresOptions } from "@/types/select";

type TerritoiresOptionsContext = TerritoiresOptions | undefined;

type TerritoiresOptionsProviderProps = {
  children: React.ReactNode;
  options: TerritoiresOptions;
};

const defaultValue = undefined;

export const TerritoiresOptionsContext =
  createContext<TerritoiresOptionsContext>(defaultValue);

export const TerritoiresOptionsProvider = ({
  children,
  options,
}: TerritoiresOptionsProviderProps) => {
  return (
    <TerritoiresOptionsContext.Provider value={options}>
      {children}
    </TerritoiresOptionsContext.Provider>
  );
};

export const useTerritoiresOptionsContext = (): TerritoiresOptionsContext =>
  useContext(TerritoiresOptionsContext);
