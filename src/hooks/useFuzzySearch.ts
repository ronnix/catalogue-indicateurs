import { useEffect, useRef, useState } from "react";

import Fuse from "fuse.js";

export function useFuzzySearch<T>(
  searchTerm: string,
  items: T[],
  options = {},
) {
  const fuse = useRef<Fuse<T>>();
  const [results, setResults] = useState<T[]>([]);

  useEffect(() => {
    fuse.current = new Fuse(items, options);
  }, [items, options]);

  useEffect(() => {
    if (fuse.current) {
      const items = fuse.current.search(searchTerm);

      setResults(items.map(({ item }) => item));
    }
  }, [searchTerm]);

  return results;
}
