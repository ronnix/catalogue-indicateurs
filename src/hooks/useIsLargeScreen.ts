import { useMemo } from "react";

import { useWindowSize } from "@uidotdev/usehooks";

export const useIsLargeScreen = () => {
  const size = useWindowSize();

  return useMemo(() => Boolean(size.width && size.width > 1024), [size.width]);
};
