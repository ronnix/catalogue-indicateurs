import { useEffect } from "react";

import { usePathname } from "next/navigation";

import { usePrevious } from "./usePrevious";

type Callback = (pathname: string) => void;

export function useOnPathnameChange(callback: Callback) {
  const pathname = usePathname();

  const previousPathname = usePrevious(pathname);

  useEffect(() => {
    if (pathname !== previousPathname) {
      callback(pathname);
    }
  }, [pathname, previousPathname, callback]);
}
