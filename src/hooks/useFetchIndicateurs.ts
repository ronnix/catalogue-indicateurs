import {
  QueryFunction,
  UseQueryOptions,
  useQueries,
} from "@tanstack/react-query";

import { QUERY_CONFIG } from "@/constants/queries";
import { NetworkError } from "@/lib/api/NetworkError";
import { extractQueryTerritoires } from "@/lib/extractQueryTerritoires";
import { getMailleLabelFromQuery } from "@/lib/getMailleLabelFromQuery";
import logger from "@/lib/logger";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

type UseQueriesOptions = UseQueryOptions<
  DefinitionIndicateur[],
  NetworkError,
  DefinitionIndicateur[],
  [{ mailles: string[]; identifiant: string }[], string, string]
>;

enum MailleId {
  DEPARTEMENT = "departement",
  REGION = "region",
  PAYS = "pays",
}

const INDICATEURS_API_URL = process.env.NEXT_PUBLIC_INDICATEURS_API_URL || "";

const ONE_VALUE_FRAGMENT = [
  "IndicateurOneValue",
  `
    valeur
    maille
    code
    __typename
  `,
];

const ROW_FRAGMENT = [
  "IndicateurRow",
  `
    row
    maille
    code
    __typename
  `,
];

const ROWS_FRAGMENT = [
  "IndicateurRows",
  `
    rows
    count
    maille
    code
    geo {
      properties
      points {
        features {
          properties
          geometry {
            type
            coordinates
          }
        }
      }
    }
    __typename
  `,
];

const LIST_FRAGMENT = [
  "IndicateurListe",
  `
    liste
    count
    maille
    code
    __typename
  `,
];

const getMailleId = (maille: string) => {
  return [MailleId.DEPARTEMENT, MailleId.REGION, MailleId.PAYS].find(
    (mailleId) => mailleId.startsWith(maille),
  );
};

const getCodeParam = (code: string, maille?: MailleId) => {
  return maille === MailleId.PAYS ? "" : `(code: "${code}")`;
};

const generateGetIndicateursQuery = ({
  data,
  maille,
  code,
}: {
  data: { mailles: string[]; identifiant: string }[];
  maille: string;
  code: string;
}) => {
  const mailleId = getMailleId(maille);
  const mailleLabel = getMailleLabelFromQuery(maille);
  const codeParam = getCodeParam(code, mailleId);

  // Some metrics are not relevant at the 'departement' or 'region'.
  // This information is contained in the  'mailles' field of the definition.
  const matchingIdentifiers = data
    .filter((entry) => entry.mailles.includes(mailleLabel))
    .map((entry) => entry.identifiant);

  const valueFragment = [
    ONE_VALUE_FRAGMENT,
    ROW_FRAGMENT,
    ROWS_FRAGMENT,
    LIST_FRAGMENT,
  ].map(([type, fields]) => {
    return `
        ... on ${type} {
          ${fields}
        }
    `;
  });

  return `
  query ${data
    .map(({ identifiant }) => identifiant)
    .join(
      "_",
    )}_${mailleId}_${code} { indicateurs(filtre: { identifiants: ["${matchingIdentifiers.join(
    '","',
  )}"] }) {
      identifiant
      nom
      description
      categorie
      schema
      unite
      datasets {
        description
        maintainer
        name
        producer {
          name
          url
        }
        table
        updateFrequency
        needsUpdate
        lastModified
        lastImport
        nextExpectedUpdate
        source
        pathPattern
        fileFormat
      }
      mailles {
        ${mailleId}${codeParam} {
          ${valueFragment}
        }
      }
    }
  }
`;
};

export type UseFetchIndicateursOptions = {
  territoires: string;
  indicateurs: Metadata[];
};

const queryFn: QueryFunction<
  DefinitionIndicateur[],
  [{ mailles: string[]; identifiant: string }[], string, string]
> = async ({ queryKey }) => {
  const [indicateurData, maille, code] = queryKey;

  const query = generateGetIndicateursQuery({
    data: indicateurData,
    maille,
    code,
  });

  const response = await fetch(INDICATEURS_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query,
    }),
  });

  if (!response.ok) {
    throw new NetworkError(
      "An error occurred while fetching indicateurs",
      response.status,
    );
  }

  const data = await response.json();

  if (!data.data) {
    logger.error({
      message: "Failed to fetch indicateurs",
      identifiers: indicateurData.map(({ identifiant }) => identifiant),
      error: data.errors,
    });

    throw new NetworkError(
      "An error occurred while fetching indicateurs",
      response.status,
    );
  }

  return data.data.indicateurs;
};

export const useFetchIndicateurs = ({
  territoires,
  indicateurs,
}: UseFetchIndicateursOptions) => {
  const parsedTerritoires = extractQueryTerritoires(territoires);

  const indicateursData = indicateurs.map(({ identifiant, mailles }) => ({
    identifiant,
    mailles,
  }));

  const queries = useQueries({
    queries: parsedTerritoires.map<UseQueriesOptions>(([maille, code]) => ({
      queryKey: [indicateursData, maille, code],
      queryFn,
      ...QUERY_CONFIG,
    })),
  });

  const error = queries.find((query) => query.error)?.error;

  return {
    data: queries,
    error,
  };
};
