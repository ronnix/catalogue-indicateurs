import { useMemo, useState } from "react";

import { FUSE_OPTIONS } from "@/constants/search";
import { Metadata } from "@/types/indicateur";

import { useFuzzySearch } from "./useFuzzySearch";

export type SearchFormSchema = {
  search: {
    value: string;
    nom: string;
    tags: { name: string; value: string }[];
    label: string;
  };
};

export const useSearchManager = (definitions: Metadata[]) => {
  const [searchTerm, setSearchTerm] = useState("");

  const results = useFuzzySearch(searchTerm, definitions, FUSE_OPTIONS);

  const options = useMemo(() => {
    const list = searchTerm !== "" ? results : definitions;

    return list.map((definition) => ({
      ...definition,
      label: definition.nom || "",
      value: definition.identifiant,
    }));
  }, [results, searchTerm, definitions]);

  const onInputChange = (input: string) => {
    setSearchTerm(input);
  };

  return { onInputChange, options, searchTerm };
};
