import { useMemo } from "react";

import { useWindowSize } from "@uidotdev/usehooks";

export const useIsMobile = () => {
  const size = useWindowSize();

  return useMemo(() => Boolean(size.width && size.width < 768), [size.width]);
};
