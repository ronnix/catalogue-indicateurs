import React from "react";

import Accordion from "@codegouvfr/react-dsfr/Accordion";

const FAQ = () => {
  return (
    <div>
      <h2>FAQ</h2>
      <Accordion className="fr-mb-3w" label="À qui est destiné ce catalogue ?">
        Ce catalogue s’adresse principalement aux agents publics cherchant à
        consulter et utiliser des données liées aux actions menées par l’ANCT.
        <br />
        <br />
        Il répertorie en effet les données liées à la majorité des programmes de
        l’ANCT pour tous les territoires français et il permet de facilement
        comparer les chiffres des différents territoires.
      </Accordion>
      <Accordion label="Quelle différence entre les fiches territoriales et le catalogue d’indicateurs ?">
        Là où{" "}
        <a
          href="https://fiches.incubateur.anct.gouv.fr/"
          target="_blank"
          rel="noopener noreferrer"
        >
          les fiches territoriales
        </a>{" "}
        ont une valeur éditoriale et de présentation, mettant en avant
        uniquement certains de nos indicateurs, le catalogue permet d’explorer
        et de visualiser toutes les données de façon plus personnalisée.
      </Accordion>
    </div>
  );
};

export default FAQ;
