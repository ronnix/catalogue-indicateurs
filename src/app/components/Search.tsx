"use client";
import React, { useMemo } from "react";

import { Button } from "@codegouvfr/react-dsfr/Button";
import { push } from "@socialgouv/matomo-next";
import clsx from "clsx";
import { useRouter } from "next/navigation";
import { Controller, useForm } from "react-hook-form";

import Select from "@/components/Select";
import { PROGRAMME_TAG_NAME } from "@/constants/indicateurs";
import { SearchFormSchema, useSearchManager } from "@/hooks/useSearchManager";
import { Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";

type SearchProps = {
  definitions: Metadata[];
};

const filterOption = () => true;

const Search = ({ definitions }: SearchProps) => {
  const router = useRouter();

  const { handleSubmit, control, formState } = useForm<SearchFormSchema>();

  const extendedDefinitions = useMemo(() => {
    return definitions.map((definition) => {
      const programme = definition.tags
        .filter((tag) => tag.name === PROGRAMME_TAG_NAME && Boolean(tag.nom))
        .map((tag) => tag.nom)
        .join(", ");

      return {
        ...definition,
        nom: `${definition.nom}${programme ? ` (${programme})` : ""}`,
      };
    });
  }, [definitions]);

  const onSubmit = (data: SearchFormSchema) => {
    const programme = data.search?.tags?.find(
      (tag) => tag.name === PROGRAMME_TAG_NAME,
    )?.value;

    push(["trackEvent", "submitSearch", data.search?.value]);

    router.push(
      `/indicateurs/${programme}?territoires=p:fr&search=${data.search?.nom}`,
    );
  };

  const { onInputChange, options, searchTerm } =
    useSearchManager(extendedDefinitions);

  return (
    <form
      className={clsx("fr-pt-3w", styles.container)}
      onSubmit={handleSubmit(onSubmit)}
    >
      <Controller
        control={control}
        rules={{ required: true }}
        name="search"
        render={({ field: { onChange, onBlur, value } }) => (
          <Select<{
            value: string;
            nom?: string | null;
            tags: { name: string; value: string }[];
            label: string;
          }>
            onChange={onChange}
            onBlur={onBlur}
            value={value}
            filterOption={filterOption}
            onInputChange={onInputChange}
            inputValue={searchTerm}
            label={
              <h2 className="fr-h4">
                <span
                  className={clsx(
                    "fr-icon-search-line fr-mr-1w",
                    styles["search-icon"],
                  )}
                />
                Zoom sur un indicateur
              </h2>
            }
            options={options}
            instanceId="homepage-search"
            containerClassName={styles.select}
            name="search"
            placeholder="Recherchez un indicateur"
            classNames={{
              control: () => styles.control,
            }}
          />
        )}
      />

      <Button
        className="fr-mb-3w fr-btn--secondary"
        type="submit"
        disabled={!formState.isValid}
      >
        Afficher
      </Button>
    </form>
  );
};

export default Search;
