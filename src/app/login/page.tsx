import React from "react";

import Breadcrumb from "@codegouvfr/react-dsfr/Breadcrumb";
import clsx from "clsx";
import { notFound } from "next/navigation";

import featureRouter from "@/lib/features/router";

import LoginForm from "./components/LoginForm";
import styles from "./styles.module.scss";

export default async function LoginPage() {
  const authEnabled = featureRouter.auth();

  if (!authEnabled) {
    return notFound();
  }

  return (
    <main className={clsx("fr-container fr-py-4w", styles.main)}>
      <Breadcrumb
        className="fr-mb-2w fr-mt-0"
        homeLinkProps={{
          href: "/",
        }}
        currentPageLabel="Login"
        segments={[]}
      />
      <h1>Se connecter</h1>
      <LoginForm />
    </main>
  );
}
