import React from "react";

import clsx from "clsx";

import commonStyles from "@/components/common/styles.module.scss";

import skeletonStyles from "./skeleton-styles.module.scss";

const Skeleton = () => {
  return (
    <>
      <div
        className={clsx(commonStyles["skeleton-element"], skeletonStyles.text)}
      />
      <div
        className={clsx(
          "fr-mt-2w",
          commonStyles["skeleton-element"],
          skeletonStyles.button,
        )}
      />
    </>
  );
};

export default Skeleton;
