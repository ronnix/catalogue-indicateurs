"use client";

import React from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import { MonCompteProButton } from "@codegouvfr/react-dsfr/MonCompteProButton";
import { signIn, signOut, useSession } from "next-auth/react";

import Skeleton from "./Skeleton";

const LoginForm = () => {
  const { data: session, status } = useSession();

  if (status === "loading") {
    return <Skeleton />;
  }

  if (session) {
    const name = session.profile
      ? ` ${session.profile.given_name} ${session.profile.family_name}`
      : "";

    return (
      <>
        <div>Bonjour {name}, vous êtes déjà identifié.</div>
        <Button
          onClick={() => signOut()}
          iconId="ri-logout-box-r-line"
          className="fr-mt-2w"
        >
          Se déconnecter
        </Button>
      </>
    );
  }

  return (
    <MonCompteProButton
      onClick={() => signIn("mon-compte-pro", { callbackUrl: "/" })}
    />
  );
};

export default LoginForm;
