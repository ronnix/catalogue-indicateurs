import "./globals.css";
import "leaflet/dist/leaflet.css";

import { Suspense } from "react";

import { DsfrHead } from "@codegouvfr/react-dsfr/next-appdir/DsfrHead";
import { DsfrProvider } from "@codegouvfr/react-dsfr/next-appdir/DsfrProvider";
import { getHtmlAttributes } from "@codegouvfr/react-dsfr/next-appdir/getHtmlAttributes";
import { Metadata } from "next";
import Link from "next/link";

import Footer from "@/components/Footer";
import Matomo from "@/components/Matomo";
import NextAuthProvider from "@/providers/NextAuthProvider";

import { defaultColorScheme } from "./defaultColorScheme";
import Providers from "./providers";
import { StartDsfr } from "./StartDsfr";

const lang = "fr";

export const metadata: Metadata = {
  title: "Catalogue d’indicateurs ANCT (beta)",
  description:
    "Le catalogue d’indicateurs permet de consulter les données liées à tous les programmes de l’ANCT pour tout le territoire.",
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html {...getHtmlAttributes({ defaultColorScheme, lang })}>
      <head>
        <StartDsfr />
        <DsfrHead
          Link={Link}
          preloadFonts={[
            "Marianne-Regular",
            "Marianne-Medium",
            "Marianne-Bold",
          ]}
        />
        <Suspense>
          <Matomo />
        </Suspense>
      </head>
      <body>
        <NextAuthProvider>
          <DsfrProvider lang={lang}>
            <Providers>{children}</Providers>
            <Footer />
          </DsfrProvider>
        </NextAuthProvider>
      </body>
    </html>
  );
}
