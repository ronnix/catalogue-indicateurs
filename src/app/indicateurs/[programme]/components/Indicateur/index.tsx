"use client";

import React, { useMemo, useRef, useState } from "react";

import { createModal } from "@codegouvfr/react-dsfr/Modal";
import Notice from "@codegouvfr/react-dsfr/Notice";
import { Chart } from "chart.js";
import clsx from "clsx";
import Link from "next/link";

import ComputationModal from "@/components/ComputationModal";
import CopyToClipboardButton from "@/components/CopyToClipboardButton";
import Tooltip from "@/components/Tooltip";
import { useTerritoiresOptionsContext } from "@/context/TerritoiresOptionsContext";
import { extractQueryTerritoires } from "@/lib/extractQueryTerritoires";
import { getAvailableViews } from "@/lib/getAvailableViews";
import { getMailleLabelFromQuery } from "@/lib/getMailleLabelFromQuery";
import {
  IndicateurViewData,
  extractIndicateurViewData,
} from "@/lib/indicateurs/extractIndicateurViewData";
import { getIndicateurType } from "@/lib/indicateurs/getIndicateurType";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";
import BarChart from "../views/BarChart";
import Map from "../views/Map";
import PieChart from "../views/PieChart";
import Table from "../views/Table";
import ViewSelector from "../ViewSelector";

type IndicateurData = { data?: DefinitionIndicateur; isLoading: boolean }[];

type IndicateurProps = {
  definition: Metadata;
  data: IndicateurData;
  territoires: string;
};

const Indicateur = ({ data, definition, territoires }: IndicateurProps) => {
  const modal = useMemo(
    () =>
      createModal({
        id: `source-modal-${definition.identifiant}`,
        isOpenedByDefault: false,
      }),
    [definition.identifiant],
  );

  const parsedTerritories = extractQueryTerritoires(territoires);

  const unavailableTerritories = parsedTerritories
    .filter(
      (territory) =>
        !definition.mailles.some((maille) => maille.startsWith(territory[0])),
    )
    .map((territory) => getMailleLabelFromQuery(territory[0]));

  const territoiresOptions = useTerritoiresOptionsContext();

  const [view, setView] = useState("table");

  const pieChartRef = useRef<Chart<"pie", (string | number)[]>>(null);
  const barChartRef = useRef<Chart<"bar", (string | number)[]>>(null);
  const mapRef = useRef<HTMLDivElement>(null);

  const type = getIndicateurType(data?.[0]?.data);

  const handleSourceClick = (event: React.MouseEvent) => {
    event.preventDefault();

    modal.open();
  };

  const viewData: IndicateurViewData[] = useMemo(
    () =>
      data
        .filter((entry) => entry.isLoading || entry.data)
        .map((entry) =>
          extractIndicateurViewData(
            entry.data,
            entry.isLoading,
            territoiresOptions,
          ),
        ),
    [data, territoiresOptions],
  );

  const availableViews = getAvailableViews(type);

  return (
    <>
      <ComputationModal modal={modal} definition={definition} />

      <div key={definition.identifiant} className="fr-mb-4w">
        <h6>
          {definition.nom}
          {definition.description && (
            <Tooltip
              className={styles.tooltip}
              title={definition.description}
              arrow
            >
              <span
                className={clsx("ri-information-fill", styles.information)}
              />
            </Tooltip>
          )}
        </h6>
        {unavailableTerritories.length > 0 && (
          <Notice
            className="fr-mb-2w"
            title={
              <>
                Cet indicateur n’est pas disponible pour les territoires
                suivants : {unavailableTerritories.join(", ")}
              </>
            }
          />
        )}
        {viewData.length > 0 && (
          <>
            <div className="fr-tabs">
              <ViewSelector
                setView={setView}
                type={type}
                view={view}
                id={definition.identifiant}
              />
              {availableViews.map((view, index) => {
                return (
                  <div
                    key={view.id}
                    id={`tabpanel-${definition.identifiant}-${index}-panel`}
                    className={clsx("fr-tabs__panel fr-p-0", styles.tabpanel)}
                    role="tabpanel"
                    aria-labelledby={`tabpanel-${definition.identifiant}-${index}`}
                    tabIndex={0}
                  >
                    {view.id === "table" && (
                      <Table data={viewData} definition={definition} />
                    )}
                    {view.id === "pie" && (
                      <PieChart
                        data={viewData}
                        definition={definition}
                        chartRef={pieChartRef}
                      />
                    )}
                    {view.id === "bar" && (
                      <BarChart
                        data={viewData}
                        definition={definition}
                        chartRef={barChartRef}
                      />
                    )}
                    {view.id === "map" && (
                      <Map data={viewData} mapRef={mapRef} />
                    )}
                  </div>
                );
              })}
            </div>
            <div className={styles.links}>
              <Link
                className="fr-link fr-text--sm"
                href="#"
                onClick={handleSourceClick}
              >
                Calcul
              </Link>

              <Link
                className="fr-link fr-text--sm fr-icon-arrow-right-line fr-link--icon-right fr-ml-2w"
                href={`/donnees?indicateurs=${definition.identifiant}`}
              >
                Source
              </Link>

              <CopyToClipboardButton
                className={styles.copy}
                data={viewData}
                definition={definition}
                view={view}
                chartRef={view === "pie" ? pieChartRef : barChartRef}
                mapRef={mapRef}
              />
            </div>
          </>
        )}
      </div>
    </>
  );
};

export default Indicateur;
