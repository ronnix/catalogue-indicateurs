import React from "react";

import {
  Chart as ChartJS,
  Title,
  ArcElement,
  Tooltip,
  ChartOptions,
  Legend,
} from "chart.js";
import { Pie } from "react-chartjs-2";

import { CHARTJS_OPTIONS } from "@/constants/chart";
import { extractChartData } from "@/lib/chart/extractChartData";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";

ChartJS.register(Title, Tooltip, ArcElement, Legend);

ChartJS.defaults.font.family = '"Marianne", arial, sans-serif';

const generateOptions = (unit?: string | null) => {
  const options: ChartOptions<"pie"> = {
    ...CHARTJS_OPTIONS,
    indexAxis: "y" as const,
    plugins: {
      tooltip: {
        xAlign: "left" as const,
        backgroundColor: "#e3e3fd",
        padding: 20,
        bodyColor: "#3a3a3a",
        titleColor: "#3a3a3a",
        callbacks: {
          label: (context) =>
            formatNumericValue({
              value: context.parsed,
              unit,
            }),
        },
      },
    },
  };

  return options;
};

type PieChartProps = {
  data: IndicateurViewData[];
  definition: Metadata;
  chartRef?: React.RefObject<ChartJS<"pie", (string | number)[]>>;
};

const PieChart = ({ data, definition, chartRef }: PieChartProps) => {
  const chartData = extractChartData(data);

  const options = generateOptions(definition.unite);

  return (
    <div className="fr-p-2w">
      <div className={styles.container}>
        <Pie data={chartData} options={options} ref={chartRef} />
      </div>
    </div>
  );
};

export default PieChart;
