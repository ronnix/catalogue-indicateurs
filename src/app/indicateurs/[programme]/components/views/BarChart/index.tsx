import React from "react";

import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  ChartOptions,
} from "chart.js";
import { Bar } from "react-chartjs-2";

import { CHARTJS_OPTIONS } from "@/constants/chart";
import { extractChartData } from "@/lib/chart/extractChartData";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { getDSFRHexaFromName } from "@/lib/getDSFRHexaFromName";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { Metadata } from "@/types/indicateur";

ChartJS.register(CategoryScale, LinearScale, BarElement, Title, Tooltip);

ChartJS.defaults.font.family = '"Marianne", arial, sans-serif';

const generateOptions = (unit?: string | null) => {
  const options: ChartOptions<"bar"> = {
    ...CHARTJS_OPTIONS,
    indexAxis: "y" as const,
    plugins: {
      legend: {
        display: false,
      },
      tooltip: {
        xAlign: "left" as const,
        padding: 20,
        backgroundColor: "#e3e3fd",
        bodyColor: "#3a3a3a",
        titleColor: "#3a3a3a",
        callbacks: {
          label: (context) =>
            formatNumericValue({
              value: context.parsed.x,
              unit,
            }),
        },
      },
    },
    scales: {
      x: {
        ticks: {
          callback: (value: number | string) =>
            formatNumericValue({ value, unit }),
          color: getDSFRHexaFromName("1000", "grey", "50"),
        },
      },
      y: {
        ticks: {
          z: 1,
          color: getDSFRHexaFromName("1000", "grey", "50"),
        },
      },
    },
  };

  return options;
};

type BarChartProps = {
  data: IndicateurViewData[];
  definition: Metadata;
  chartRef?: React.RefObject<ChartJS<"bar", (string | number)[]>>;
};

const BarChart = ({ data, definition, chartRef }: BarChartProps) => {
  const chartData = extractChartData(data);

  const options = generateOptions(definition.unite);

  return (
    <div className="fr-p-2w">
      <Bar data={chartData} options={options} ref={chartRef} />
    </div>
  );
};

export default BarChart;
