import React from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import clsx from "clsx";

import BaseTable from "@/components/Table";
import { TABLE_HEADERS } from "@/constants/table";
import { useListModalContext } from "@/context/ListModalContext";
import { formatNumericValue } from "@/lib/formatNumericValue";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";

type TableProps = {
  data: IndicateurViewData[];
  definition: Metadata;
};

const ViewListCell = ({
  item,
  definition,
}: {
  item: string[] | Record<string, unknown>[];
  definition: Metadata;
}) => {
  const { open, setData } = useListModalContext();

  return (
    <Button
      priority="tertiary no outline"
      size="small"
      iconId="ri-eye-line"
      onClick={() => {
        setData(item, definition);
        open();
      }}
      title="Voir la liste"
    />
  );
};

const Table = ({ data, definition }: TableProps) => {
  const unit = definition.unite;

  const hasList = data?.[0]?.hasList;

  return (
    <BaseTable<string | number | string[] | Record<string, unknown>[]>
      headers={[...TABLE_HEADERS, ...(hasList ? ["Voir la liste"] : [])]}
      data={data.map(({ label, count, updatedAt, list }) => {
        const rowData: (
          | string
          | number
          | string[]
          | Record<string, unknown>[]
        )[] = [label, count, updatedAt];

        if (hasList && list) {
          rowData.push(list);
        }

        return rowData;
      })}
      renderCell={(item, index) => {
        let value: string | number | React.ReactNode = "-";

        if (typeof item === "string" || typeof item === "number") {
          value =
            index === 1 ? formatNumericValue({ value: item, unit }) : item;
        }

        if (Array.isArray(item)) {
          value = <ViewListCell item={item} definition={definition} />;
        }

        return <td key={`${item}_${index}`}>{value}</td>;
      }}
      className={clsx("fr-pt-0 fr-mb-0", styles.table)}
    />
  );
};

export default Table;
