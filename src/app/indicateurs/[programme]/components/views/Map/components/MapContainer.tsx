import React, { useEffect } from "react";

import { MapContainer as BaseMapContainer, useMap } from "react-leaflet";

import { usePrevious } from "@/hooks/usePrevious";

import styles from "./styles.module.scss";

type MapProps = {
  center?: [number, number];
  bounds?: [number, number][];
  zoom?: number;
  whenReady?: () => void;
  children: React.ReactNode;
};

const FitBoundsManager = ({ bounds }: { bounds: [number, number][] }) => {
  const map = useMap();

  const previousLength = usePrevious(bounds?.length || 0);
  useEffect(() => {
    if (bounds.length !== previousLength) {
      map.fitBounds(bounds, {
        maxZoom: 10,
      });
    }
  }, [bounds, previousLength, map]);

  return null;
};

const MapContainer = ({
  bounds,
  center,
  children,
  zoom,
  whenReady,
}: MapProps) => {
  return (
    <div>
      <BaseMapContainer
        bounds={bounds}
        boundsOptions={{
          maxZoom: 10,
        }}
        zoom={zoom}
        center={center}
        className={styles.map}
        whenReady={whenReady}
      >
        {bounds && <FitBoundsManager bounds={bounds} />}
        {children}
      </BaseMapContainer>
    </div>
  );
};

export default MapContainer;
