import clsx from "clsx";
import { divIcon } from "leaflet";
import { Marker as BaseMarker } from "react-leaflet";

type MarkerProps = {
  position: [number, number];
  children?: React.ReactNode;
  className?: string;
};

const INLINE_MARKER =
  '<svg xmlns="http://www.w3.org/2000/svg" fill="currentColor" viewBox="0 0 24 24" width="24" height="24"><path d="M18.364 3.636a9 9 0 0 1 0 12.728L12 22.728l-6.364-6.364A9 9 0 0 1 18.364 3.636ZM12 8a2 2 0 1 0 0 4 2 2 0 0 0 0-4Z"/></svg>';

const Marker = ({ position, children, className }: MarkerProps) => {
  const MarkerIcon = divIcon({
    iconSize: [24, 24],
    className: clsx(className),
    html: INLINE_MARKER,
  });

  return (
    <BaseMarker position={position} icon={MarkerIcon}>
      {children}
    </BaseMarker>
  );
};

export default Marker;
