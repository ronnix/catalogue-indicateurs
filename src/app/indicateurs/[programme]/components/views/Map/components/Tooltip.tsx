import { Tooltip as BaseTooltip } from "react-leaflet";

type TooltipProps = {
  children: React.ReactNode;
  permanent?: boolean;
  keyProp?: string;
};

const Tooltip = ({ children, keyProp, permanent }: TooltipProps) => {
  return (
    <BaseTooltip
      key={`${keyProp}-${permanent}`}
      direction="top"
      offset={[0, -10]}
      permanent={permanent}
    >
      {children}
    </BaseTooltip>
  );
};

export default Tooltip;
