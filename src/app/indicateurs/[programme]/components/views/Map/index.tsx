import React, {
  ChangeEventHandler,
  MutableRefObject,
  useState,
  memo,
} from "react";

import { Notice } from "@codegouvfr/react-dsfr/Notice";
import isEqual from "lodash/isEqual";
import dynamic from "next/dynamic";
import Link from "next/link";

import Checkbox from "@/components/Checkbox";
import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";
import { extractCoordinates } from "@/lib/map/extractCoordinates";
import { isDuplicate } from "@/lib/map/isDuplicate";

import Legend from "./Legend";
import styles from "./styles.module.scss";

const FRANCE_CENTER_COORDINATES: [number, number] = [46.2276, 2.2137];

const MapContainer = dynamic(() => import("./components/MapContainer"), {
  ssr: false,
});
const Marker = dynamic(() => import("./components/Marker"), { ssr: false });
const Tile = dynamic(() => import("./components/Tile"), { ssr: false });
const Tooltip = dynamic(() => import("./components/Tooltip"), { ssr: false });

type MapProps = {
  data: IndicateurViewData[];
  mapRef: MutableRefObject<HTMLDivElement | null>;
};

const arePropsEqual = (prevProps: MapProps, nextProps: MapProps) => {
  return isEqual(prevProps, nextProps);
};

const Map = memo(({ data, mapRef }: MapProps) => {
  const [tooltipsVisible, setTooltipsVisibility] = useState(false);

  const { bounds, departementsCoordinates, regionsCoordinates } =
    extractCoordinates(data);

  const isLoading = data.some(({ isLoading }) => isLoading);

  const mapProps:
    | { zoom: number; center: [number, number]; bounds?: never }
    | { bounds: [number, number][]; zoom?: never; center?: never } =
    !bounds || bounds.length === 0
      ? { zoom: 5, center: FRANCE_CENTER_COORDINATES }
      : { bounds };

  if ((!bounds || bounds.length === 0) && !isLoading) {
    return <div>Aucune donnée à afficher.</div>;
  }

  const onTooltipCheckboxChange: ChangeEventHandler<HTMLInputElement> = (
    event,
  ) => {
    const { checked } = event.target;

    setTooltipsVisibility(checked);
  };

  return (
    <div className="fr-p-2w">
      <div ref={mapRef}>
        <Legend data={data} />
        <MapContainer {...mapProps}>
          <Tile />
          {data.map(({ geoData, maille }, index) =>
            (geoData || []).map((geoData) => {
              if (
                isDuplicate(
                  geoData.coordinates,
                  maille,
                  departementsCoordinates,
                  regionsCoordinates,
                )
              ) {
                return null;
              }

              return (
                <Marker
                  key={geoData.coordinates.join("")}
                  position={geoData.coordinates}
                  className={styles[`pin-${index + 1}`]}
                >
                  <Tooltip
                    keyProp={geoData.coordinates.join("")}
                    permanent={tooltipsVisible}
                  >
                    {geoData.label}
                  </Tooltip>
                </Marker>
              );
            }),
          )}
        </MapContainer>
      </div>
      <Checkbox
        small
        label="Afficher toutes les info-bulles"
        name="tooltip"
        onChange={onTooltipCheckboxChange}
        checked={tooltipsVisible}
        className="fr-mt-1w"
      />
      <Notice
        className="fr-mt-2w"
        title={
          <>
            Cette carte est une carte de travail, retrouvez les cartes
            officielles sur{" "}
            <Link
              target="_blank"
              rel="noreferrer"
              href="https://cartotheque.anct.gouv.fr/cartes"
            >
              la cartothèque
            </Link>{" "}
            de l’ANCT
          </>
        }
      />
    </div>
  );
}, arePropsEqual);

Map.displayName = "Map";

export default Map;
