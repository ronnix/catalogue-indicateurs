import clsx from "clsx";

import { IndicateurViewData } from "@/lib/indicateurs/extractIndicateurViewData";

import styles from "./styles.module.scss";

type LegendProps = {
  data: IndicateurViewData[];
};

const Legend = ({ data }: LegendProps) => {
  return (
    <div className={clsx(styles["legend-container"], "fr-pb-1w")}>
      {data.map(({ label }, index) => (
        <div key={index} className={clsx("fr-text--xs", styles["legend-item"])}>
          <div className={clsx(styles[`legend-${index + 1}`], styles.legend)} />
          {label}
        </div>
      ))}
    </div>
  );
};

export default Legend;
