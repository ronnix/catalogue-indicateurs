"use client";

import React, { memo } from "react";

import { UseQueryResult } from "@tanstack/react-query";
import clsx from "clsx";
import isEqual from "lodash/isEqual";

import { HIDDEN_METRICS } from "@/constants/indicateurs";
import { NetworkError } from "@/lib/api/NetworkError";
import { mapIndicateurData } from "@/lib/indicateurs/mapIndicateurData";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";

import styles from "./styles.module.scss";
import Indicateur from "../Indicateur";

type ListProps = {
  definitions: Metadata[];
  territoires: string;
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[];
  error?: NetworkError | null;
  totalCount: number;
  programme?: string;
};

const arePropsEqual = (prevProps: ListProps, nextProps: ListProps) => {
  const hasError = !prevProps.error && nextProps.error;

  if (hasError) {
    return false;
  }

  const prevDefinitionIds = prevProps.definitions
    .map(({ identifiant }) => identifiant)
    .sort((a, b) => (a > b ? -1 : 1));

  const nextDefinitionIds = nextProps.definitions
    .map(({ identifiant }) => identifiant)
    .sort((a, b) => (a > b ? -1 : 1));

  const hasFreshDefinitions = !isEqual(prevDefinitionIds, nextDefinitionIds);

  if (hasFreshDefinitions) {
    return false;
  }

  const hasFreshData = prevProps.data.some((entry, index) => {
    return !entry.data && nextProps.data?.[index].data;
  });

  if (hasFreshData) {
    return false;
  }

  const hasFreshTerritories = prevProps.territoires !== nextProps.territoires;

  if (hasFreshTerritories) {
    return false;
  }

  return true;
};

const List = memo(
  ({
    definitions,
    territoires,
    totalCount,
    programme,
    data,
    error,
  }: ListProps) => {
    if (territoires.length === 0) {
      return (
        <section className={clsx(styles.content, "fr-pb-1w fr-pt-4w fr-pr-4w")}>
          <div>
            Il y a {totalCount} indicateur{totalCount !== 1 ? "s" : ""} “
            {programme}” dans le catalogue
          </div>
          <ul>
            {definitions.map((definition) => (
              <li key={definition.identifiant} className="fr-text--bold">
                {definition.nom}
              </li>
            ))}
          </ul>
          <div className="fr-mt-2w">
            Pour afficher les résultats par territoire, choisissez un ou
            plusieurs territoire dans le menu de droite.
          </div>
        </section>
      );
    }

    if (definitions.length === 0) {
      return (
        <section className={clsx(styles.content, "fr-pb-1w fr-pt-4w fr-pr-4w")}>
          <h6>Aucun indicateur ne correspond à votre recherche.</h6>
        </section>
      );
    }

    if (error) {
      return (
        <section className={clsx(styles.content, "fr-pb-1w fr-pt-4w fr-pr-4w")}>
          <h6>
            Une erreur est survenue lors du chargement des données des
            indicateurs.
          </h6>
        </section>
      );
    }

    return (
      <section className={clsx(styles.content, "fr-pb-1w fr-pr-4w")}>
        <div className="fr-pt-4w">
          {definitions
            .filter(
              (definition) => !HIDDEN_METRICS.includes(definition.identifiant),
            )
            .map((definition) => {
              const indicateurData = mapIndicateurData(
                definition.identifiant,
                data,
              );

              return (
                <Indicateur
                  key={definition.identifiant}
                  definition={definition}
                  data={indicateurData}
                  territoires={territoires}
                />
              );
            })}
        </div>
      </section>
    );
  },
  arePropsEqual,
);

List.displayName = "List";

export default List;
