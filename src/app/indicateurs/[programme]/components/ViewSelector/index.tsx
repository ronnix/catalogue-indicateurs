"use client";

import React from "react";

import clsx from "clsx";

import { IndicateurType, getAvailableViews } from "@/lib/getAvailableViews";
import { Visualization } from "@/types/visualization";

import styles from "./styles.module.scss";

function ViewSelector({
  setView,
  view,
  type,
  id,
}: {
  setView: (type: Visualization) => void;
  view: string;
  type: IndicateurType;
  id: string;
}) {
  const availableViews = getAvailableViews(type);

  return (
    <ul
      className={clsx("fr-tabs__list", styles.list)}
      role="tablist"
      aria-label="Sélectionner la vue"
    >
      {availableViews.map((item, index) => (
        <li role="presentation" key={item.id}>
          <button
            id={`tabpanel-${id}-${index}`}
            className={clsx("fr-tabs__tab fr-tabs__tab--icon-left", item.icon)}
            tabIndex={0}
            role="tab"
            aria-selected={view === item.id}
            aria-controls={`tabpanel-${id}-${index}-panel`}
            onClick={() => setView(item.id)}
          >
            {item.label}
          </button>
        </li>
      ))}
    </ul>
  );
}

export default ViewSelector;
