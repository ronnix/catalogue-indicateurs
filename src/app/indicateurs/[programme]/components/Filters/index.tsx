import React, { ChangeEvent, useCallback, useMemo } from "react";

import Button from "@codegouvfr/react-dsfr/Button";
import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/navigation";

import Menu from "@/components/Menu";
import { extractQueryTerritoires } from "@/lib/extractQueryTerritoires";
import { generateMetricsListPath } from "@/lib/path/generateMetricsListPath";
import { extractSelectValues } from "@/lib/select/extractSelectValue";
import { Programme } from "@/types/programme";
import { TerritoireOption, TerritoiresOptions } from "@/types/select";

import styles from "./styles.module.scss";

type FiltersProps = {
  territoiresOptions: TerritoiresOptions;
  territoires: string;
  programme?: Programme;
  searchTerm: string;
  setSearchTerm: (term: string) => void;
  clearSearch: () => void;
  filteredIndicateursCount: number;
  totalIndicateursCount: number;
  titleComponent: React.ReactNode;
};

export default function Filters({
  territoiresOptions,
  territoires,
  programme,
  searchTerm,
  setSearchTerm,
  clearSearch,
  filteredIndicateursCount,
  totalIndicateursCount,
  titleComponent,
}: FiltersProps) {
  const router = useRouter();

  const handleSearchTermChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSearchTerm(event.target.value);
    },
    [setSearchTerm],
  );

  const onTerritoireChange = (values: TerritoireOption[]) => {
    const path = generateMetricsListPath({
      programme: programme?.identifiant,
      territoires: values,
      search: searchTerm,
    });

    router.push(path, {
      scroll: false,
    });
  };

  const selectValues = useMemo(() => {
    const selected = extractQueryTerritoires(territoires);

    return extractSelectValues(territoiresOptions, (option) => {
      return selected.some(
        ([maille, code]) =>
          option.value === code && option.maille[0] === maille,
      );
    });
  }, [territoires, territoiresOptions]);

  const territoiresLabel =
    selectValues.length > 0
      ? `${selectValues.length} Territoire${
          selectValues.length !== 1 ? "s" : ""
        } sélectionné${selectValues.length !== 1 ? "s" : ""}`
      : "Régions, départements";

  return (
    <div className={clsx("fr-col-12 fr-col-lg-4 fr-py-0", styles.container)}>
      {titleComponent}
      <nav
        className="fr-sidemenu fr-sidemenu--right fr-sidemenu--sticky-full-height fr-pl-0"
        role="navigation"
      >
        <div className={clsx("fr-sidemenu__inner", styles.content)}>
          <button
            className="fr-sidemenu__btn"
            hidden
            aria-controls="fr-sidemenu-wrapper"
            aria-expanded="false"
          >
            Filtrer et comparer
          </button>
          <div>
            {selectValues.length === 0 && (
              <div className="fr-text--lg fr-text--bold fr-mb-2w">
                Quels territoires souhaitez-vous observer ?
              </div>
            )}
            <div className={clsx("fr-mb-1w fr-text--lg", styles.weighted)}>
              Sélectionner un ou plusieurs territoires
            </div>
            <Menu
              id="territories"
              label={territoiresLabel}
              options={territoiresOptions}
              value={selectValues}
              onChange={onTerritoireChange}
            />
            {selectValues.length > 0 && (
              <div className={styles["link-container"]}>
                <Link
                  className="fr-link fr-text--sm"
                  href={generateMetricsListPath({
                    programme: programme?.identifiant,
                    search: searchTerm,
                  })}
                >
                  Supprimer tous les territoires
                </Link>
              </div>
            )}
          </div>
          <hr className={clsx("fr-my-4w", styles.separator)} />
          <div>
            <div className={clsx("fr-mb-1w fr-text--lg", styles.weighted)}>
              Filtrer par mot-clé
            </div>
            <div className={styles["search-container"]}>
              <input
                className="fr-input"
                placeholder="Rechercher"
                value={searchTerm}
                onChange={handleSearchTermChange}
              />
            </div>
            {searchTerm && (
              <div className={clsx("fr-mt-1w", styles["link-container"])}>
                <Link
                  className="fr-link fr-text--sm"
                  onClick={clearSearch}
                  href="#"
                >
                  Supprimer le filtre
                </Link>
              </div>
            )}
            <div className="fr-my-1w">
              {filteredIndicateursCount !== totalIndicateursCount
                ? `${filteredIndicateursCount} indicateurs affichés sur `
                : ""}
              {totalIndicateursCount} indicateurs disponibles
            </div>
          </div>
          <div className={clsx("fr-mb-2w", styles.cta)}>
            <Button
              linkProps={{
                href: "mailto:donnees@anct.gouv.fr",
              }}
              iconPosition="right"
              iconId="ri-mail-line"
            >
              Signaler une erreur
            </Button>
          </div>
        </div>
      </nav>
    </div>
  );
}
