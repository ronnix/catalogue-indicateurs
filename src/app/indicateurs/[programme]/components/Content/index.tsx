"use client";

import React, { useEffect, useMemo, useState } from "react";

import clsx from "clsx";
import debounce from "lodash/debounce";

import PageTitle from "@/components/PageTitle";
import { HIDDEN_METRICS } from "@/constants/indicateurs";
import { TerritoiresOptionsProvider } from "@/context/TerritoiresOptionsContext";
import { useFetchIndicateurs } from "@/hooks/useFetchIndicateurs";
import { useFuzzySearch } from "@/hooks/useFuzzySearch";
import { Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";
import { TerritoiresOptions } from "@/types/select";

import styles from "./styles.module.scss";
import Filters from "../Filters";
import List from "../List";

export const SEARCH_DELAY = 400;

export const FUSE_OPTIONS = {
  keys: [{ name: "nom", weight: 2 }, "identifiant"],
  threshold: 0.35,
};

type ContentProps = {
  indicateurs: Metadata[];
  programme?: Programme;
  territoiresOptions: TerritoiresOptions;
  territoires: string;
  search: string;
};

const Content = ({
  programme,
  indicateurs,
  territoiresOptions,
  territoires,
  search,
}: ContentProps) => {
  const [searchState, setSearchState] = useState({
    term: search,
    filter: search,
  });

  const debouncedSetSearchFilter = useMemo(
    () =>
      debounce((term) => {
        setSearchState((prevState) => ({ ...prevState, filter: term }));
      }, SEARCH_DELAY),
    [],
  );

  const clearSearch = () => {
    setSearchState({
      term: "",
      filter: "",
    });
  };

  const setSearchTerm = (term: string) => {
    setSearchState((prevState) => ({ ...prevState, term }));
  };

  useEffect(() => {
    debouncedSetSearchFilter(searchState.term);
  }, [debouncedSetSearchFilter, searchState.term]);

  const results = useFuzzySearch(searchState.filter, indicateurs, FUSE_OPTIONS);

  const visibleIndicateurs =
    searchState.filter.length > 0 ? results : indicateurs;

  const { data, error } = useFetchIndicateurs({
    territoires,
    indicateurs,
  });

  const filteredIndicateursCount = visibleIndicateurs.filter(
    (metric) => !HIDDEN_METRICS.includes(metric.identifiant),
  ).length;

  const totalIndicateursCount = indicateurs.filter(
    (metric) => !HIDDEN_METRICS.includes(metric.identifiant),
  ).length;

  return (
    <TerritoiresOptionsProvider options={territoiresOptions}>
      <div className={clsx("fr-col-12 fr-col-lg-8", styles.main)}>
        <PageTitle
          className={styles.title}
          title={programme?.nom || ""}
          searchTerm={searchState.term}
          definitions={visibleIndicateurs}
          data={data}
        />

        <List
          error={error}
          definitions={visibleIndicateurs}
          totalCount={totalIndicateursCount}
          territoires={territoires}
          data={data}
          programme={programme?.nom}
        />
      </div>
      <Filters
        programme={programme}
        totalIndicateursCount={totalIndicateursCount}
        territoiresOptions={territoiresOptions}
        territoires={territoires}
        searchTerm={searchState.term}
        setSearchTerm={setSearchTerm}
        clearSearch={clearSearch}
        filteredIndicateursCount={filteredIndicateursCount}
        titleComponent={
          <PageTitle
            className={styles["filters-title"]}
            title={programme?.nom || ""}
            searchTerm={searchState.term}
            definitions={visibleIndicateurs}
            data={data}
          />
        }
      />
    </TerritoiresOptionsProvider>
  );
};

export default Content;
