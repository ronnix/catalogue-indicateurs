import Header from "@/components/Header";
import { ListModalProvider } from "@/context/ListModalContext";

export default function IndicateursLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <ListModalProvider>
      <Header />
      {children}
    </ListModalProvider>
  );
}
