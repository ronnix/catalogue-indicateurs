import React from "react";

import { Breadcrumb } from "@codegouvfr/react-dsfr/Breadcrumb";

import { fetchDatasets } from "@/lib/api/datasets";
import { fetchDefinitions } from "@/lib/api/indicateurs";

import Content from "./components/Content";

type DatasetsProps = {
  searchParams: {
    search?: string;
    indicateurs?: string;
  };
};

const Datasets = async ({ searchParams }: DatasetsProps) => {
  const { search, indicateurs } = searchParams;

  const datasets = await fetchDatasets();
  const definitions = await fetchDefinitions();

  return (
    <main className="fr-grid-row fr-container">
      <Breadcrumb
        className="fr-col-12"
        currentPageLabel="Données"
        homeLinkProps={{
          href: "/",
        }}
        segments={[]}
      />
      <Content
        search={search || ""}
        datasets={datasets}
        definitions={definitions}
        indicateurs={indicateurs || ""}
      />
    </main>
  );
};

export default Datasets;
