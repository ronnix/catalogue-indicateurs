import React from "react";

import clsx from "clsx";
import Link from "next/link";

import Maintainer from "@/components/indicateurs/Maintainer";
import Producer from "@/components/Producer";
import { PROGRAMME_TAG_NAME } from "@/constants/indicateurs";
import { formatDate } from "@/lib/formatDate";
import { formatPeriodicity } from "@/lib/formatPeriodicity";
import { removeInvalidIndicateurs } from "@/lib/indicateurs/removeInvalidIndicateurs";
import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";

type SidebarProps = {
  dataset: DatasetWithIndicateurs;
};

const Sidebar = ({ dataset }: SidebarProps) => {
  const periodicity = formatPeriodicity(dataset.updateFrequency);

  const filteredMetrics = removeInvalidIndicateurs(dataset.indicateurs);

  return (
    <section className={clsx("fr-col-12 fr-col-lg-4 fr-mb-6w")}>
      <div className={clsx("fr-py-2w", styles.block)}>
        <div className="fr-text--bold">
          <span className={clsx("ri-calendar-2-line", styles.icon)} /> Dernière
          mise à jour
        </div>
        <div>{formatDate(dataset.lastModified)}</div>
      </div>
      {dataset.producer?.name && (
        <div className={clsx("fr-py-2w", styles.block)}>
          <div className="fr-text--bold">
            <span className={clsx("ri-global-line", styles.icon)} /> Source
          </div>
          <div className={styles.block}>
            <Producer producer={dataset.producer} prefix={false} />
          </div>
        </div>
      )}
      <div className={clsx("fr-py-2w", styles.block)}>
        <div className="fr-text--bold">
          <span className={clsx("ri-refresh-line", styles.icon)} /> Périodicité
        </div>
        <div>{periodicity || "-"}</div>
      </div>
      <div className={clsx("fr-py-2w", styles.block)}>
        <div className="fr-text--bold">
          <span className={clsx("ri-user-line", styles.icon)} /> Contributeur
        </div>
        <div>{<Maintainer maintainer={dataset.maintainer || "-"} />}</div>
      </div>
      <div className={clsx("fr-py-2w", styles.block)}>
        <div className="fr-text--bold">
          <span className={clsx("ri-line-chart-line", styles.icon)} />{" "}
          Indicateurs liés
        </div>
        <div>
          {filteredMetrics.length > 0
            ? filteredMetrics.map((indicateur) => {
                const programme = indicateur.tags?.find(
                  (tag) => tag.name === PROGRAMME_TAG_NAME,
                )?.value;

                return (
                  <Link
                    className="fr-link fr-mr-1w"
                    href={`/indicateurs/${programme}?territoires=p:fr&search=${indicateur.nom}`}
                    key={indicateur.identifiant}
                  >
                    {indicateur.nom}
                  </Link>
                );
              })
            : "Aucun"}
        </div>
      </div>
    </section>
  );
};

export default Sidebar;
