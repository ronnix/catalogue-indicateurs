import React from "react";

import clsx from "clsx";

import Producer from "@/components/Producer";
import SourceInfo from "@/components/SourceInfo";
import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";

type ContentProps = {
  dataset: DatasetWithIndicateurs;
};

const Content = ({ dataset }: ContentProps) => {
  return (
    <section className={clsx("fr-col-12 fr-col-lg-8 fr-mb-6w fr-pr-10w")}>
      <h3 className={clsx("fr-mb-2w", styles.title)}>{dataset.name}</h3>
      {dataset.producer?.name && <Producer producer={dataset.producer} />}

      {dataset.description && <div>{dataset.description}</div>}

      <div className="fr-mt-2w fr-mb-1w fr-text--lg fr-text--bold">
        Ces données sont produites à partir de :
      </div>
      <SourceInfo dataset={dataset} />
    </section>
  );
};

export default Content;
