import React from "react";

import { Breadcrumb } from "@codegouvfr/react-dsfr/Breadcrumb";
import { Button } from "@codegouvfr/react-dsfr/Button";
import clsx from "clsx";
import { notFound } from "next/navigation";

import { fetchDatasets } from "@/lib/api/datasets";

import Content from "./components/Content";
import Sidebar from "./components/Sidebar";
import styles from "./styles.module.scss";

type DatasetProps = {
  params: {
    dataset: string;
  };
};

const Dataset = async ({ params }: DatasetProps) => {
  const datasets = await fetchDatasets();

  const dataset = datasets.find((dataset) => dataset.table === params.dataset);

  if (!dataset) {
    return notFound();
  }

  return (
    <>
      <main className="fr-grid-row fr-container">
        <Breadcrumb
          className="fr-col-12"
          currentPageLabel={dataset.name}
          homeLinkProps={{
            href: "/",
          }}
          segments={[
            {
              label: "Données",
              linkProps: {
                href: "/donnees",
              },
            },
          ]}
        />
        <Content dataset={dataset} />
        <Sidebar dataset={dataset} />
      </main>
      <div className={clsx("fr-py-4w fr-mb-0", styles.contribution)}>
        <div className="fr-container">
          <div className="fr-col-12 fr-col-lg-8 fr-pr-10w">
            <h4 className={styles.white}>Vous êtes contributeur ?</h4>
            <div className={clsx(styles.white, "fr-mb-2w")}>
              Producteur, responsable, contributeur n’hésitez pas à nous
              contacter pour toutes demandes
            </div>
            <Button
              linkProps={{
                href: "mailto:donnees@anct.gouv.fr",
              }}
              className={styles.button}
              iconPosition="right"
              iconId="ri-mail-line"
              priority="secondary"
            >
              Contacter l’équipe
            </Button>
          </div>
        </div>
      </div>
    </>
  );
};

export default Dataset;
