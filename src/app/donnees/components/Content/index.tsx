"use client";

import React, { useEffect, useMemo, useState } from "react";

import clsx from "clsx";
import debounce from "lodash/debounce";

import { useFuzzySearch } from "@/hooks/useFuzzySearch";
import { Metadata } from "@/types/indicateur";
import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";
import Filters from "../Filters";
import List from "../List";

export const SEARCH_DELAY = 400;

export const FUSE_OPTIONS = {
  keys: [{ name: "name", weight: 2 }, "table"],
  threshold: 0.35,
};

type ContentProps = {
  datasets: DatasetWithIndicateurs[];
  definitions: Metadata[];
  search: string;
  indicateurs: string;
};

const Content = ({
  search,
  datasets,
  definitions,
  indicateurs,
}: ContentProps) => {
  const [searchState, setSearchState] = useState({
    term: search,
    filter: search,
  });

  const debouncedSetSearchFilter = useMemo(
    () =>
      debounce((term) => {
        setSearchState((prevState) => ({ ...prevState, filter: term }));
      }, SEARCH_DELAY),
    [],
  );

  const clearSearch = () => {
    setSearchState({
      term: "",
      filter: "",
    });
  };

  const setSearchTerm = (term: string) => {
    setSearchState((prevState) => ({ ...prevState, term }));
  };

  useEffect(() => {
    debouncedSetSearchFilter(searchState.term);
  }, [debouncedSetSearchFilter, searchState.term]);

  const results = useFuzzySearch(searchState.filter, datasets, FUSE_OPTIONS);

  const searchFilteredDatasets =
    searchState.filter.length > 0 ? results : datasets;

  const metricsIds = indicateurs ? indicateurs.split(",") : [];

  const visibleDatasets =
    metricsIds.length > 0
      ? searchFilteredDatasets.filter((dataset) =>
          dataset.indicateurs.find((indicateur) =>
            metricsIds.includes(indicateur.identifiant),
          ),
        )
      : searchFilteredDatasets;

  return (
    <>
      <div className={clsx("fr-col-12 fr-col-lg-8", styles.main)}>
        <List datasets={visibleDatasets} totalCount={datasets.length} />
      </div>
      <Filters
        searchTerm={searchState.term}
        setSearchTerm={setSearchTerm}
        clearSearch={clearSearch}
        definitions={definitions}
        indicateurs={indicateurs}
      />
    </>
  );
};

export default Content;
