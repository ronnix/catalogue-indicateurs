import React, { ChangeEvent, useCallback, useMemo } from "react";

import Tag from "@codegouvfr/react-dsfr/Tag";
import clsx from "clsx";
import Link from "next/link";
import { useRouter } from "next/navigation";
import { MultiValue } from "react-select";

import Select from "@/components/Select";
import { PROGRAMME_TAG_NAME } from "@/constants/indicateurs";
import { useSearchManager } from "@/hooks/useSearchManager";
import { generateDatasetsListPath } from "@/lib/path/generateDatasetsListPath";
import { extractSelectValues } from "@/lib/select/extractSelectValue";
import { Metadata } from "@/types/indicateur";
import { Option } from "@/types/select";

import styles from "./styles.module.scss";

type FiltersProps = {
  searchTerm: string;
  setSearchTerm: (term: string) => void;
  clearSearch: () => void;
  definitions: Metadata[];
  indicateurs: string;
};

const filterOption = () => true;

export default function Filters({
  searchTerm,
  setSearchTerm,
  clearSearch,
  definitions,
  indicateurs,
}: FiltersProps) {
  const router = useRouter();

  const extendedDefinitions = useMemo(() => {
    return definitions.map((definition) => {
      const programme = definition.tags
        .filter((tag) => tag.name === PROGRAMME_TAG_NAME && Boolean(tag.nom))
        .map((tag) => tag.nom)
        .join(", ");

      return {
        ...definition,
        nom: `${definition.nom}${programme ? ` (${programme})` : ""}`,
      };
    });
  }, [definitions]);

  const {
    onInputChange,
    options,
    searchTerm: metricsSearchTerm,
  } = useSearchManager(extendedDefinitions);

  const handleSearchTermChange = useCallback(
    (event: ChangeEvent<HTMLInputElement>) => {
      setSearchTerm(event.target.value);
    },
    [setSearchTerm],
  );

  const onChange = (values: MultiValue<Option>) => {
    const path = generateDatasetsListPath({
      indicateurs: values,
    });

    router.push(path, {
      scroll: false,
    });
  };

  const selectValues = useMemo(() => {
    const selected = indicateurs.split(",");

    return extractSelectValues(options, (option) =>
      selected.some((identifiant) => option.value === identifiant),
    );
  }, [options, indicateurs]);

  const onTagClose = (option: Option) => {
    const updatedValues = selectValues.filter(
      (value) => value.value !== option.value,
    );

    const path = generateDatasetsListPath({
      indicateurs: updatedValues,
    });

    router.push(path, {
      scroll: false,
    });
  };

  return (
    <div className={clsx("fr-col-12 fr-col-lg-4 fr-py-0", styles.container)}>
      <nav
        className="fr-sidemenu fr-sidemenu--right fr-sidemenu--sticky-full-height fr-pl-0"
        role="navigation"
      >
        <div className={clsx("fr-sidemenu__inner", styles.content)}>
          <button
            className="fr-sidemenu__btn"
            hidden
            aria-controls="fr-sidemenu-wrapper"
            aria-expanded="false"
          >
            Filtrer
          </button>
          <div>
            <div className={clsx("fr-text--lg", styles.weighted)}>
              Filtrer par indicateur
            </div>
            <div className={clsx("fr-mb-1w fr-text--xs", styles.hint)}>
              Pour afficher les jeux de données liés aux indicateurs
            </div>
            <div className={styles["search-container"]}>
              <Select<
                {
                  value: string;
                  nom?: string | null;
                  tags: { name: string; value: string }[];
                  label: string;
                },
                true
              >
                onChange={onChange}
                value={selectValues}
                isMulti
                filterOption={filterOption}
                onInputChange={onInputChange}
                inputValue={metricsSearchTerm}
                options={options}
                instanceId="metrics-search"
                name="search"
                placeholder="Rechercher"
                classNames={{
                  control: () => styles.control,
                  valueContainer: () => styles["value-container"],
                }}
                containerClassName={clsx(
                  "fr-mb-1w",
                  styles["select-container"],
                )}
              />

              <div>
                {selectValues.map((value) => (
                  <Tag
                    className="fr-mr-1w"
                    key={value.value}
                    dismissible
                    nativeButtonProps={{
                      onClick: () => {
                        onTagClose(value);
                      },
                    }}
                    small
                  >
                    {value.label.length > 20
                      ? `${value.label.slice(0, 20)}…`
                      : value.label}
                  </Tag>
                ))}
              </div>
            </div>
          </div>
          <div className="fr-mt-3w">
            <div className={clsx("fr-mb-1w fr-text--lg", styles.weighted)}>
              Filtrer par mot-clé
            </div>
            <div className={styles["search-container"]}>
              <input
                className={clsx("fr-input", styles["search"])}
                placeholder="Rechercher"
                value={searchTerm}
                onChange={handleSearchTermChange}
              />
            </div>
            {searchTerm && (
              <div className={clsx("fr-mt-1w", styles["link-container"])}>
                <Link
                  className="fr-link fr-text--sm"
                  onClick={clearSearch}
                  href="#"
                >
                  Supprimer le filtre
                </Link>
              </div>
            )}
          </div>
        </div>
      </nav>
    </div>
  );
}
