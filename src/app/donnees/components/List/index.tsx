import React from "react";

import clsx from "clsx";

import { DatasetWithIndicateurs } from "@/types/schema";

import styles from "./styles.module.scss";
import DatasetView from "../DatasetView";

type ListProps = {
  datasets: DatasetWithIndicateurs[];
  totalCount: number;
};

const List = ({ datasets, totalCount }: ListProps) => {
  const currentCount = datasets.length;

  const countMessage =
    totalCount === currentCount
      ? `${totalCount} jeux de données`
      : `${currentCount} jeux de données affichés sur ${totalCount} disponibles`;

  return (
    <section className={clsx(styles.content, "fr-pb-1w fr-pr-4w")}>
      <div className="fr-text fr-mb-2w">{countMessage}</div>

      {datasets.map((dataset) => (
        <DatasetView key={dataset.table} dataset={dataset} />
      ))}
    </section>
  );
};

export default List;
