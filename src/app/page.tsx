import clsx from "clsx";
import Image from "next/image";
import Link from "next/link";

import Header from "@/components/Header";
import { fetchDefinitions } from "@/lib/api/indicateurs";
import { fetchProgrammes, getProgrammesOptions } from "@/lib/api/programmes";

import FAQ from "./components/FAQ";
import Search from "./components/Search";
import styles from "./styles.module.scss";

export default async function Home() {
  const programmes = await fetchProgrammes();
  const definitions = await fetchDefinitions();
  const programmesOptions = getProgrammesOptions(programmes, definitions);

  return (
    <>
      <Header />
      <main>
        <section className={clsx("fr-py-8w", styles.hero)}>
          <div className="fr-container fr-grid-row">
            <div className={clsx("fr-col-12 fr-col-lg-5")}>
              <h1 className="fr-mb-2w">
                Des indicateurs au service de vos besoins
              </h1>
              <ul>
                <li>Consulter les indicateurs d’un programme</li>
                <li>Comparer sur plusieurs mailles géographiques</li>
                <li>Copier facilement sur des documents externes</li>
              </ul>
            </div>
            <div className={clsx("fr-col-12 fr-col-lg-7", styles.centered)}>
              <div className={styles["image-container"]}>
                <Image
                  src="/artwork/home.png"
                  alt="Ordinateur portable et données"
                  fill
                />
              </div>
            </div>
          </div>
        </section>

        <section className="fr-pt-5w fr-container">
          <h2>Sélectionner le programme</h2>
          <div className={styles.groups}>
            {programmesOptions.map((group) => (
              <div key={group.label}>
                <h5 className={styles["group-title"]}>{group.label}</h5>
                {group.options
                  .sort((a, b) => (a.label > b.label ? 1 : -1))
                  .map((programme) => (
                    <div
                      className={clsx(
                        "fr-tile fr-tile--sm fr-tile--no-icon fr-tile--horizontal fr-enlarge-link",
                        styles.tile,
                      )}
                      key={programme.value}
                    >
                      <div className="fr-tile__body">
                        <h6 className="fr-tile__title">
                          <Link href={`/indicateurs/${programme.value}`}>
                            {programme.label}
                          </Link>
                        </h6>
                      </div>
                    </div>
                  ))}
              </div>
            ))}
          </div>
        </section>

        <section className={clsx(styles["search-container"])}>
          <div className="fr-container fr-py-4w">
            <div className={clsx("fr-highlight fr-ml-0", styles.highlight)}>
              <Search definitions={definitions} />
            </div>
          </div>
        </section>

        <section className={clsx("fr-py-5w fr-mt-2w", styles.faq)}>
          <div className="fr-container">
            <FAQ />
          </div>
        </section>
      </main>
    </>
  );
}
