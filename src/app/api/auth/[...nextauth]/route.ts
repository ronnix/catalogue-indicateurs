import NextAuth from "next-auth";

const MON_COMPTE_PRO_PROVIDER_ID = "mon-compte-pro";

const handler = NextAuth({
  secret: process.env.NEXTAUTH_SECRET,
  callbacks: {
    async session({ session, token }) {
      session.accessToken = token.accessToken;
      session.profile = token.profile;
      return session;
    },
    async jwt({ token, account, profile }) {
      if (account) {
        token.accessToken = account.access_token;
      }

      if (profile) {
        token.profile = profile;
      }

      return token;
    },
  },
  providers: [
    {
      id: MON_COMPTE_PRO_PROVIDER_ID,
      name: "MonComptePro",
      type: "oauth",
      wellKnown: `${process.env.MON_COMPTE_PRO_PROVIDER_URL}/.well-known/openid-configuration`,
      authorization: {
        params: { scope: "openid email profile organizations" },
      },
      idToken: true,
      userinfo: {
        // Was not able to override this declaration.
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        async request(context: any) {
          if (!context.tokens.access_token) {
            return {};
          }

          const userinfo = await context.client.userinfo(
            context.tokens.access_token,
          );

          return userinfo;
        },
      },
      checks: ["pkce", "state"],
      clientId: process.env.MON_COMPTE_PRO_OIDC_CLIENT_ID,
      clientSecret: process.env.MON_COMPTE_PRO_OIDC_CLIENT_SECRET,
      profile({ sub, ...rest }) {
        return { id: sub, ...rest };
      },
    },
  ],
});

export { handler as GET, handler as POST };
