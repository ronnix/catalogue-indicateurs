import { DateTime } from "luxon";
import { NextResponse } from "next/server";

import logger from "@/lib/logger";

type DocumentData = Record<string, number>;
type Workspace = { name: string; docs: Record<string, string>[] };

export const revalidate = 43200;

/**
 * This grafana query calculates the number of unique
 * users connected to grist over a period of 24 hours.
 */
const GRIST_STATS_QUERY =
  'count(count by(email) (rate({namespace="grist", container="grist"} |= `email=` | regexp `.*email=(?P<email>[^ ]+), .*` [24h])))';

const GRAFANA_API_URL = `https://grafana.incubateur.anct.gouv.fr/api/ds/query?ds_type=loki&requestId=explore_qK6`;
const GRIST_API_URL = ` https://grist.incubateur.anct.gouv.fr/api/orgs/3`;

const GRAFANA_ACCESS_TOKEN = process.env.GRAFANA_ACCESS_TOKEN || "";
const GRIST_ACCESS_TOKEN = process.env.GRIST_ACCESS_TOKEN || "";

export async function GET() {
  const from = String(DateTime.now().minus({ days: 30 }).toMillis());
  const to = String(DateTime.now().minus({ seconds: 10 }).toMillis());

  const data = {
    grafana: null,
    users: 0,
    documents: 0,
  };

  try {
    const grafanaResponse = await fetch(GRAFANA_API_URL, {
      method: "POST",
      body: JSON.stringify({
        queries: [
          {
            refId: "A",
            expr: GRIST_STATS_QUERY,
            queryType: "range",
            datasource: {
              type: "loki",
              uid: "P_dLvhWVz",
            },
            editorMode: "builder",
            step: "24h",
            maxLines: 1000,
            legendFormat: "",
            datasourceId: 14,
            intervalMs: 1800000,
            maxDataPoints: 1496,
          },
        ],
        from,
        to,
      }),
      headers: {
        Authorization: `Bearer ${GRAFANA_ACCESS_TOKEN}`,
        "Content-Type": "application/json",
      },
    });

    const grafanaData = await grafanaResponse.json();

    if (!grafanaResponse.ok) {
      logger.error({
        message: "An error occurred while fetching grist stats from grafana.",
        status: grafanaResponse.status,
      });

      return NextResponse.json(
        {
          message: "An error occurred while fetching grist stats from grafana.",
        },
        { status: grafanaResponse.status },
      );
    }

    data.grafana = grafanaData;
  } catch (error: unknown) {
    logger.error({
      message: "An error occurred while fetching grist stats from grafana.",
      error,
    });

    return NextResponse.json(
      { message: "An error occurred while fetching grist stats from grafana." },
      { status: 500 },
    );
  }

  try {
    const gristWorkspacesResponse = await fetch(`${GRIST_API_URL}/workspaces`, {
      headers: {
        Authorization: `Bearer ${GRIST_ACCESS_TOKEN}`,
      },
    });

    const gristWorkspacesData = await gristWorkspacesResponse.json();

    const documents = gristWorkspacesData.reduce(
      (data: DocumentData, workspace: Workspace) => {
        const docs = workspace.docs.length;
        return {
          ...data,
          [workspace.name]: docs,
          total: data.total + docs,
        };
      },
      { total: 0 },
    );

    data.documents = documents;
  } catch (error: unknown) {
    logger.error({
      message: "An error occurred while fetching grist workspaces.",
      error,
    });

    return NextResponse.json(
      { message: "An error occurred while fetching grist workspaces." },
      { status: 500 },
    );
  }

  try {
    const gristAccessResponse = await fetch(`${GRIST_API_URL}/access`, {
      headers: {
        Authorization: `Bearer ${GRIST_ACCESS_TOKEN}`,
      },
    });

    const gristAccessData: { users: unknown[] } =
      await gristAccessResponse.json();

    const users = gristAccessData.users.length;

    data.users = users;
  } catch (error: unknown) {
    logger.error({
      message: "An error occurred while fetching grist users.",
      error,
    });

    return NextResponse.json(
      { message: "An error occurred while fetching grist users." },
      { status: 500 },
    );
  }

  logger.info({
    message: "Generated stats for grist",
    data,
    from,
    to,
  });

  return NextResponse.json(data, {
    status: 200,
    headers: {
      "Access-Control-Allow-Methods": "GET, OPTIONS",
      "Access-Control-Allow-Headers": "Content-Type, Authorization",
      "Access-Control-Allow-Origin": "*",
    },
  });
}
