import { NextResponse } from "next/server";

import logger from "@/lib/logger";

const COMMUNES_API_URL = process.env.NEXT_PUBLIC_COMMUNES_API_URL || "";
const EPCIS_API_URL = process.env.NEXT_PUBLIC_EPCIS_API_URL || "";

const EPCI_PREFIXES = ["CA", "CC", "CU"];

type GeocodingResponse = {
  nom: string;
  code: string;
  centre: {
    type: "Point";
    coordinates: [number, number];
  };
}[];

const generateTerritoireParam = (maille: string, code: string) => {
  const key = maille === "region" ? "codeRegion" : "codeDepartement";

  return `${key}=${code}`;
};

export async function GET(request: Request) {
  const { searchParams } = new URL(request.url);
  const search = searchParams.getAll("search") || "";
  const maille = searchParams.get("maille") || "";
  const code = searchParams.get("code") || "";

  try {
    const responses = await Promise.all(
      search.map((name) => {
        const api = EPCI_PREFIXES.includes(name.slice(0, 2))
          ? EPCIS_API_URL
          : COMMUNES_API_URL;

        return fetch(
          `${api}?nom=${name}&fields=centre&limit=5&${generateTerritoireParam(
            maille,
            code,
          )}`,
        );
      }),
    );

    const erroredResponse = responses.find((response) => !response.ok);

    if (erroredResponse) {
      const { status } = erroredResponse;

      logger.error({
        message: "An error occurred while fetching communes coordinates.",
        error: erroredResponse,
        status,
      });

      return NextResponse.json(
        { message: "An error occurred while fetching communes coordinates." },
        { status },
      );
    }

    const data: ([number, number] | undefined)[] = await Promise.all(
      responses.map(async (response, index) => {
        const data: GeocodingResponse = await response.json();

        // The communes API does not always return an exact name match as the first result
        // so we will try to manually look for an exact match and fallback to the first item.
        const match =
          data.find((entry) => entry.nom === search[index]) || data[0];

        if (!match) {
          return;
        }

        const { coordinates } = match.centre;

        return [coordinates[1], coordinates[0]];
      }),
    );

    return NextResponse.json(data);
  } catch (error: unknown) {
    logger.error({
      message: "An error occurred while fetching communes coordinates.",
      error,
    });

    return NextResponse.json(
      { message: "An error occurred while fetching communes coordinates." },
      { status: 500 },
    );
  }
}
