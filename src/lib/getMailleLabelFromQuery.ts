export enum MailleLabel {
  DEPARTEMENT = "département",
  REGION = "région",
  PAYS = "pays",
}

export const getMailleLabelFromQuery = (maille: string) => {
  return (
    [MailleLabel.DEPARTEMENT, MailleLabel.REGION, MailleLabel.PAYS].find(
      (mailleId) => mailleId.startsWith(maille),
    ) || MailleLabel.DEPARTEMENT
  );
};
