import { MAX_BAR_THICKNESS } from "@/constants/chart";
import { COLORS } from "@/constants/visualization";

import { getDSFRHexaFromName } from "../getDSFRHexaFromName";
import { IndicateurViewData } from "../indicateurs/extractIndicateurViewData";

export const extractChartData = (data: IndicateurViewData[]) => {
  const { values, backgroundColor, labels } = data.reduce<{
    labels: string[];
    backgroundColor: string[];
    values: (string | number)[];
  }>(
    (chartData, entry, index) => {
      const { label, count } = entry;

      const color = getDSFRHexaFromName(COLORS[index % 17]);

      chartData.backgroundColor.push(color);
      chartData.labels.push(String(label));
      chartData.values.push(String(count));

      return chartData;
    },
    { values: [], labels: [], backgroundColor: [] },
  );

  return {
    labels,
    datasets: [
      {
        maxBarThickness: MAX_BAR_THICKNESS,
        label: "",
        data: values,
        backgroundColor,
      },
    ],
  };
};
