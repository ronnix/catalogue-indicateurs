import { Option, OptionGroup } from "@/types/select";

export function extractSelectValue<OptionType extends Option>(
  options: (OptionGroup<OptionType> | OptionType)[],
  matcher: (option: OptionType) => boolean,
) {
  return options.reduce<OptionType | undefined>((match, groupOrOption) => {
    if (match) {
      return match;
    }

    if (!("options" in groupOrOption)) {
      const isMatch = matcher(groupOrOption);
      return isMatch ? groupOrOption : undefined;
    }

    return groupOrOption.options.find(matcher);
  }, undefined);
}

export function extractSelectValues<OptionType extends Option>(
  options: (OptionType | OptionGroup<OptionType>)[],
  matcher: (option: OptionType | OptionGroup<OptionType>) => boolean,
) {
  return options.reduce<(OptionType | OptionGroup<OptionType>)[]>(
    (matches, groupOrOption) => {
      const isMatch = matcher(groupOrOption);

      if (!("options" in groupOrOption)) {
        return isMatch ? [...matches, groupOrOption] : matches;
      }

      return [
        ...matches,
        ...(isMatch ? [groupOrOption] : []),
        ...groupOrOption.options.filter(matcher),
      ];
    },
    [],
  );
}
