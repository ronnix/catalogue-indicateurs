export const extractQueryTerritoires = (query: string) => {
  if (!query) {
    return [];
  }

  return query.split(",").map((territoire) => territoire.split(":"));
};
