export const FEATURES_CONFIG = {
  auth: {
    enabled: true,
    decision: () => {
      return Boolean(process.env.AUTH_ENABLED);
    },
  },
};
