import { FEATURES_CONFIG } from "./config";
import createFeatureRouter from "./routerFactory";

const featureRouter = createFeatureRouter(FEATURES_CONFIG);

export default featureRouter;
