/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * We are defining arbitrary function types for inference in
 * this file, so we have to make use of any in several places.
 */

type FeatureConfig = {
  enabled: boolean;
  decision: (...args: any[]) => any;
};

function createFeatureRouter<T extends Record<keyof T, FeatureConfig>>(
  arg: T,
): {
  [K in keyof T]: T[K]["decision"] extends () => infer R
    ? () => R
    : T[K]["decision"] extends (...args: infer A) => infer R
    ? (...args: A) => R
    : never;
};
/**
 * Creates a Feature Router with the provided feature config.
 *
 * You can specify arbitrary function signatures for the 'decision'
 * function depending on what is going to drive the feature logic.
 *
 * @param featureConfig Feature configuration
 * @example ```javascript
 *  const config ={
 *      some_feature: {
 *        enabled: true,
 *        decision: (param: string) => param === 'foo'
 *      }
 *  }
 *
 *  const featureRouter = createFeatureRouter(config);
 *
 *  export default featureRouter;
 *
 * // another-file.js
 *  if (featureRouter.some_feature('foo')) {
 *      // enabled feature logic
 *  }
 * ```
 */
function createFeatureRouter(config: Record<string, FeatureConfig>) {
  return Object.fromEntries(
    Object.entries(config).map(([key, { enabled, decision }]) => [
      key,
      (...args: any[]) => {
        if (!enabled) {
          return false;
        }

        if (!decision) {
          return true;
        }

        return decision(...args);
      },
    ]),
  );
}

export default createFeatureRouter;
