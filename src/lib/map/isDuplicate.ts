export const isDuplicate = (
  value: [number, number],
  maille: string,
  departements: [number, number][],
  regions: [number, number][],
) => {
  let collection: [number, number][] = [];

  if (maille === "pays") {
    collection = [...departements, ...regions];
  }
  if (maille === "région") {
    collection = departements;
  }

  return collection.some(
    (entry) => entry[0] === value[0] && entry[1] === value[1],
  );
};
