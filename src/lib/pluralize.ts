const exceptions = [
  {
    match: "territorial(aux)",
    plural: "territoriaux",
    singular: "territorial",
  },
  { match: "du(des)", plural: "des", singular: "du" },
  { match: "de la(des)", plural: "des", singular: "de la" },
];

const PLURAL_REGEX = /\(s\)/g;

export const pluralize = (label: string | null | undefined, count: number) => {
  if (!label) {
    return;
  }

  let pluralizedLabel = label;

  const isPlural = count !== 1;

  for (const { match, plural, singular } of exceptions) {
    pluralizedLabel = pluralizedLabel.replace(
      match,
      isPlural ? plural : singular,
    );
  }

  return pluralizedLabel.replace(PLURAL_REGEX, isPlural ? "s" : "");
};
