import { Option } from "@/types/select";

export function generateDatasetsListPath({
  indicateurs = [],
}: {
  indicateurs?: Option[] | readonly Option[];
}) {
  const searchParams = new URLSearchParams([
    [
      "indicateurs",
      indicateurs.map((indicateur) => indicateur.value).join(","),
    ],
  ]);

  return `/donnees?${searchParams}`;
}
