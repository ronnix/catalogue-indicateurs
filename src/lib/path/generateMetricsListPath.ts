import { TerritoireOption } from "@/types/select";

export function generateMetricsListPath({
  programme = "",
  territoires = [],
  search = "",
}: {
  programme?: string;
  territoires?: readonly TerritoireOption[];
  search?: string;
}) {
  const territoiresParam = territoires
    .map((territoire) => `${territoire.maille[0]}:${territoire.value}`)
    .join(",");

  const searchParams = new URLSearchParams([
    ["territoires", territoiresParam],
    ["search", search],
  ]);

  return `/indicateurs/${programme}?${searchParams}`;
}
