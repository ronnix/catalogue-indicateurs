import { Source } from "@/types/dataset";
import { DatasetWithIndicateurs } from "@/types/schema";

const GRIST_SLUG = "grist";
const AIRTABLE_SLUG = "airtable";
const METABASE_SLUG = "metabase";
const DATA_GOUV_SLUG = "data.gouv";

export const extractSource = (dataset: DatasetWithIndicateurs) => {
  if (dataset.source?.includes(GRIST_SLUG)) {
    return Source.GRIST;
  }

  if (dataset.source?.includes(METABASE_SLUG)) {
    return Source.METABASE;
  }

  if (dataset.source?.includes(AIRTABLE_SLUG)) {
    return Source.AIRTABLE;
  }

  if (dataset.source?.includes(DATA_GOUV_SLUG)) {
    return Source.DATA_GOUV;
  }

  return Source.MANUAL;
};
