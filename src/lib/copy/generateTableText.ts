export const generateTableText = <T extends { title: string }>(
  columns: T[],
  lines: (string | number)[][],
  label?: string,
) => {
  const header = `${columns.map((column) => column.title).join("\t")}\n`;

  const body = lines.map((values) => values.join("\t")).join("\n");

  return `${label ? `${label}\n\n` : ""}${header}${body}`;
};
