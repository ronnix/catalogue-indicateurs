export const hasClipboardSupport = () => {
  return (
    typeof navigator !== "undefined" &&
    typeof navigator.clipboard.write !== "undefined" &&
    typeof ClipboardItem !== "undefined"
  );
};
