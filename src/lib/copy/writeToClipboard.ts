import * as clipboardPolyfill from "clipboard-polyfill";

export const writeToClipboard = async (html: string, text: string) => {
  const htmlBlob = new Blob([html], { type: "text/html" });
  const textBlob = new Blob([text], { type: "text/plain" });

  const { Item, clipboard } = getClipboard();

  await clipboard.write([
    new Item({
      [htmlBlob.type]: htmlBlob,
      [textBlob.type]: textBlob,
    }) as ClipboardItem,
  ]);
};

export const getClipboard = () => {
  // The clipboard API for writing custom content types is not yet
  // available on firefox so we have to polyfill it in such cases
  const clipboard =
    typeof navigator.clipboard.write !== "undefined"
      ? navigator.clipboard
      : clipboardPolyfill;

  const Item = (
    typeof ClipboardItem !== "undefined"
      ? ClipboardItem
      : clipboardPolyfill.ClipboardItem
  ) as typeof ClipboardItem;

  return { Item, clipboard };
};
