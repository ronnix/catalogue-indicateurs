export async function getImageBlobFromUrl(url: string): Promise<Blob> {
  const fetchedImageData = await fetch(url);

  const blob = await fetchedImageData.blob();

  return blob;
}
