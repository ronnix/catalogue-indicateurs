export const generateListClipboardContent = (
  list: string[],
  label?: string,
) => {
  return list.reduce(
    (content, item) => ({
      html: `${content.html}<br>${item}`,
      text: `${content.html}\n${item}`,
    }),
    { html: `${label}<br><br>`, text: `${label}\n\n` },
  );
};
