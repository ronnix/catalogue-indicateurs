import DOMToImage from "dom-to-image";

import { IMAGE_MIME_TYPE } from "@/constants/image";

import { getImageBlobFromUrl } from "./getImageBlobFromUrl";
import { getClipboard } from "./writeToClipboard";

export const copyMapToClipboard = async (
  ref: React.RefObject<HTMLDivElement | null>,
) => {
  if (!ref.current) {
    return;
  }

  const src = await DOMToImage.toPng(ref.current);

  const blob = await getImageBlobFromUrl(src);

  const { Item, clipboard } = getClipboard();

  await clipboard.write([
    new Item({
      [IMAGE_MIME_TYPE]: blob,
    }) as ClipboardItem,
  ]);
};
