import { ReadonlyURLSearchParams } from "next/navigation";

import { writeToClipboard } from "./writeToClipboard";

export const copyLinkToClipboard = async (
  searchTerm: string,
  searchParams: ReadonlyURLSearchParams,
) => {
  const { origin, pathname } = window?.location || {};

  const territoires = searchParams.get("territoires");

  const updatedParams = new URLSearchParams([
    ["territoires", territoires || ""],
    ["search", searchTerm || ""],
  ]);

  const url = `${origin}${pathname}?${updatedParams}`;

  await writeToClipboard(url, url);
};
