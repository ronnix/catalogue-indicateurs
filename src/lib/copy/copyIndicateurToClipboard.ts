import { UseQueryResult } from "@tanstack/react-query";

import { TABLE_HEADERS } from "@/constants/table";
import { DefinitionIndicateur, Metadata } from "@/types/indicateur";
import { TerritoiresOptions } from "@/types/select";

import { generateListClipboardContent } from "./generateListClipboardContent";
import { generateTableMarkup } from "./generateTableMarkup";
import { generateTableText } from "./generateTableText";
import { writeToClipboard } from "./writeToClipboard";
import { NetworkError } from "../api/NetworkError";
import {
  extractIndicateurViewData,
  IndicateurViewData,
} from "../indicateurs/extractIndicateurViewData";
import { extractTitledHeaders } from "../table/extractTitledHeaders";

export const generateClipboardMarkup = (
  clipboardData: {
    data: IndicateurViewData[];
    name?: string | null;
  }[],
) => {
  const tables = clipboardData.map((metric) =>
    generateTableMarkup(
      TABLE_HEADERS.map((header) => ({ title: header })),
      metric.data.map(({ label, count, updatedAt }) => [
        label,
        count,
        updatedAt,
      ]) as (string | number)[][],
      metric.name || "",
    ),
  );

  return tables.join("<br><br>");
};

export const generateClipboardText = (
  clipboardData: {
    data: IndicateurViewData[];
    name?: string | null;
  }[],
) => {
  const tables = clipboardData.map((metric) =>
    generateTableText(
      TABLE_HEADERS.map((header) => ({ title: header })),
      metric.data.map(({ label, count, updatedAt }) => [
        label,
        count,
        updatedAt,
      ]) as (string | number)[][],
      metric.name || "",
    ),
  );

  return tables.join("\n\n");
};

export const copyListToClipboard = async (
  data: string[] | Record<string, unknown>[],
  definition: Metadata,
) => {
  const label = definition.nom ?? "";
  let html: string;
  let text: string;

  if (typeof data[0] === "string") {
    ({ html, text } = generateListClipboardContent(data as string[], label));
  } else {
    const columns = extractTitledHeaders(definition.schema);
    const lines = (data as Record<string, string | number>[]).map((entry) =>
      columns.map((column) => entry[column.key] || ""),
    );

    html = generateTableMarkup(columns, lines, label);
    text = generateTableText(columns, lines, label);
  }

  await writeToClipboard(html, text);
};

export const copyIndicateurToClipboard = async (
  data: IndicateurViewData[],
  label: string,
) => {
  const headers = TABLE_HEADERS.map((header) => ({ title: header }));
  const tableData = data.map(({ label, count, updatedAt }) => [
    label,
    count,
    updatedAt,
  ]) as (string | number)[][];

  const html = generateTableMarkup(headers, tableData, label);
  const text = generateTableText(headers, tableData, label);

  await writeToClipboard(html, text);
};

export const copyIndicateursToClipboard = async (
  definitions: Metadata[],
  data: UseQueryResult<DefinitionIndicateur[], NetworkError>[],
  territoires?: TerritoiresOptions,
) => {
  const clipboardData = definitions.map((definition) => {
    const indicateurData = data.map((territoire) => {
      const territoireData = territoire.data?.find(
        (result) => result.identifiant === definition.identifiant,
      );
      const indicateurData = extractIndicateurViewData(
        territoireData,
        territoire.isFetching,
        territoires,
      );
      return indicateurData;
    }, []);

    return { data: indicateurData, name: definition.nom };
  });

  const html = generateClipboardMarkup(clipboardData);
  const text = generateClipboardText(clipboardData);

  await writeToClipboard(html, text);
};
