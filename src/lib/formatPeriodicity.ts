const PERIODICITY_MAP = {
  quinquennial: "Quinquennale",
  quadriennial: "Quadriennale",
  triennial: "Trienniale",
  biennial: "Biennale",
  annual: "Annuelle",
  semiannual: "Semestrielle",
  threeTimesAYear: "Quadrimestrielle",
  quarterly: "Trimestrielle",
  bimonthly: "Bimestrielle",
  monthly: "Mensuelle",
  semimonthly: "Bimensuelle",
  threeTimesAMonth: "Trimensuelle",
  biweekly: "Quinzomadaire",
  weekly: "Hebdomadaire",
  semiweekly: "Bihebdomadaire",
  threeTimesAWeek: "Trihebdomadaire",
  fourTimesAWeek: "4 fois par semaine",
  daily: "Quotidienne",
  semidaily: "Biquotidienne",
  threeTimesADay: "3 fois par jour",
  fourTimesADay: "4 fois par jour",
  hourly: "Horaire",
  stable: "Stable",
  unknown: "Inconnue",
  punctual: "Ponctuelle",
  continuous: "Continue",
  irregular: "Irrégulière",
};

export const formatPeriodicity = (code?: string | null) => {
  if (!code) {
    return;
  }

  return code in PERIODICITY_MAP
    ? PERIODICITY_MAP[code as keyof typeof PERIODICITY_MAP]
    : code;
};
