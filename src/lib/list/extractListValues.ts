export const extractListValues = (
  values: string[] | { libelle: string; centre: Record<string, number> }[],
) => {
  if (!values) {
    return;
  }

  return values.map((value) =>
    typeof value === "string" ? value : value.libelle,
  );
};
