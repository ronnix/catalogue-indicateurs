import {
  NarrowedTypeIndicateur,
  isList,
  isGeoList,
  isRows,
} from "./typeGuards";

export const getCount = (indicateur: NarrowedTypeIndicateur): number => {
  if (isList(indicateur) || isRows(indicateur) || isGeoList(indicateur)) {
    return indicateur.count;
  }

  return indicateur.valeur;
};
