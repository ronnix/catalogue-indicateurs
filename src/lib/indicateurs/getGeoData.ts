import { NarrowedTypeIndicateur, isGeoList } from "./typeGuards";

export type GeoData = { coordinates: [number, number]; label: string }[];

export const getGeoData = (
  indicateur: NarrowedTypeIndicateur,
): GeoData | null => {
  if (isGeoList(indicateur)) {
    return (
      indicateur.geo?.points?.features.map((feature) => {
        const { coordinates } = feature.geometry as {
          coordinates: [number, number];
        };
        const { geo__libelle } = feature.properties as { geo__libelle: string };

        return {
          coordinates: [coordinates[1], coordinates[0]],
          label: geo__libelle,
        };
      }) ?? null
    );
  }

  return null;
};
