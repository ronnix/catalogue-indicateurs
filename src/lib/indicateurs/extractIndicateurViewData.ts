import { MAILLE_MAP } from "@/constants/mailles";
import { DefinitionIndicateur } from "@/types/indicateur";
import { TerritoiresOptions } from "@/types/select";

import { getCount } from "./getCount";
import { GeoData, getGeoData } from "./getGeoData";
import { getIndicateurType } from "./getIndicateurType";
import { getList } from "./getList";
import { NarrowedTypeIndicateur } from "./typeGuards";
import { getMatchingOptions } from "../getMatchingOptions";
import { getUpdatedAt } from "../getUpdatedAt";

export type IndicateurViewData = {
  label: string;
  count: number | string;
  updatedAt: string;
  list: string[] | Record<string, unknown>[] | string | null;
  geoData: GeoData | null;
  isLoading: boolean;
  maille: string;
  hasList: boolean;
};

export const extractIndicateurViewData = (
  data: DefinitionIndicateur | undefined,
  isLoading: boolean,
  territoires: TerritoiresOptions | undefined,
): IndicateurViewData => {
  const type = getIndicateurType(data);

  if (isLoading || !data) {
    return {
      label: "-",
      count: "-",
      updatedAt: "-",

      list: "-",
      geoData: null,
      isLoading: true,
      maille: "",
      hasList: [
        "IndicateurRows",
        "IndicateurListe",
        "IndicateurListeGeo",
      ].includes(type),
    };
  }

  const { mailles } = data;

  const content = mailles.region || mailles.departement || mailles.pays;

  const matchingOptions = getMatchingOptions(content, territoires);

  const territoireLabel =
    matchingOptions?.find(
      (option) =>
        option.value === content.code &&
        MAILLE_MAP[option.maille] === content.maille,
    )?.label ?? "-";

  const count = getCount(content as NarrowedTypeIndicateur);

  const list = getList(content as NarrowedTypeIndicateur);

  const geoData = getGeoData(content as NarrowedTypeIndicateur);

  const updatedAt =
    getUpdatedAt(data.datasets)?.toLocaleDateString("fr-FR", {
      day: "numeric",
      month: "numeric",
      year: "numeric",
    }) ?? "-";

  return {
    label: territoireLabel,
    count,
    updatedAt,
    list,
    geoData,
    isLoading,
    maille: content.maille,
    hasList: Boolean(list),
  };
};
