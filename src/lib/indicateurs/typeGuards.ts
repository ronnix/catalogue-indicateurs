import {
  IndicateurListe,
  IndicateurOneValue,
  IndicateurRows,
} from "@/types/schema";

export type NarrowedTypeIndicateur =
  | IndicateurOneValue
  | IndicateurRows
  | IndicateurListe;

export const isOneValue = (
  data: NarrowedTypeIndicateur,
): data is IndicateurOneValue => {
  return data.__typename === "IndicateurOneValue";
};

export const isList = (
  data: NarrowedTypeIndicateur,
): data is IndicateurListe => {
  return data.__typename === "IndicateurListe";
};

export const isRows = (
  data: NarrowedTypeIndicateur,
): data is IndicateurRows => {
  return data.__typename === "IndicateurRows" && !data.geo;
};

export const isGeoList = (
  data: NarrowedTypeIndicateur,
): data is IndicateurRows => {
  return data.__typename === "IndicateurRows" && Boolean(data.geo);
};
