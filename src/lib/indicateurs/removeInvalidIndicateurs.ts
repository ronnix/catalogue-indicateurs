import { DEPRECATED_METRICS } from "@/constants/indicateurs";
import { DefinitionIndicateur } from "@/types/schema";

export const removeInvalidIndicateurs = (indicateurs: DefinitionIndicateur[]) =>
  indicateurs.filter(
    (indicateur) =>
      indicateur.identifiant &&
      indicateur.nom &&
      !DEPRECATED_METRICS.includes(indicateur.identifiant),
  );
