import { DefinitionIndicateur } from "@/types/indicateur";

import { NarrowedTypeIndicateur, isGeoList, isRows } from "./typeGuards";

export const getIndicateurType = (definition?: DefinitionIndicateur) => {
  if (!definition) {
    return "IndicateurOneValue";
  }

  const { mailles } = definition ?? {};
  const content = (mailles?.region ||
    mailles?.departement ||
    mailles?.pays ||
    {}) as NarrowedTypeIndicateur;

  if (isGeoList(content)) {
    return "IndicateurListeGeo";
  }

  if (isRows(content)) {
    return "IndicateurRows";
  }

  return content.__typename || "IndicateurOneValue";
};
