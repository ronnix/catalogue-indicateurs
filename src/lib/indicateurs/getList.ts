import {
  NarrowedTypeIndicateur,
  isGeoList,
  isList,
  isRows,
} from "./typeGuards";

export const getList = (
  indicateur: NarrowedTypeIndicateur,
): string[] | Record<string, unknown>[] | null => {
  if (isList(indicateur)) {
    return indicateur.liste;
  }

  if (isRows(indicateur) || isGeoList(indicateur)) {
    return indicateur.rows;
  }

  return null;
};
