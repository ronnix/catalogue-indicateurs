import * as Sentry from "@sentry/nextjs";
import { revalidateTag } from "next/cache";

import { DatasetWithIndicateurs } from "@/types/schema";

import logger from "../logger";

const INDICATEURS_API_URL = process.env.NEXT_PUBLIC_INDICATEURS_API_URL || "";
const INSITU_ACCESS_TOKEN = process.env.INSITU_ACCESS_TOKEN || "";

const DATASETS_FETCH_REVALIDATE_DELAY = 3600;

const GET_DATASETS_QUERY = `query getDatasets {
  datasets {
    name
    description
    pathPattern
    source
    fileFormat
    table
    maintainer
    lastModified
    lastImport
    updateFrequency
    nextExpectedUpdate
    needsUpdate
    producer {
      url
      name
    }
    indicateurs {
      nom
      identifiant
      tags {
        name
        value
      }
    }
  }
}`;

export async function fetchDatasets() {
  const response = await fetch(INDICATEURS_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      access_token: INSITU_ACCESS_TOKEN,
    },
    body: JSON.stringify({
      query: GET_DATASETS_QUERY,
    }),
    next: {
      revalidate: DATASETS_FETCH_REVALIDATE_DELAY,
      tags: ["datasets"],
    },
  });

  const result: {
    data: {
      datasets: DatasetWithIndicateurs[];
    } | null;
    error?: unknown;
  } = await response.json();

  if (!result.data) {
    logger.error({
      message: "Failed to fetch datasets, trying to refetch on next request.",
      error: result.error,
    });

    Sentry.withScope((scope) => {
      scope.setExtra("cause", "Failed to fetch datasets.");
      Sentry.captureException(result.error);
    });

    revalidateTag("datasets");

    return [];
  }

  return result.data.datasets;
}
