import { CATEGORIES } from "@/constants/categories";
import { PROGRAMMES } from "@/constants/programmes";
import { Metadata } from "@/types/indicateur";
import { Programme } from "@/types/programme";
import { Option } from "@/types/select";

export async function fetchProgrammes() {
  return PROGRAMMES;
}

function sortByCategorie(programmes: Programme[], definitions: Metadata[]) {
  return programmes.reduce<{
    aos: Option[];
    ville: Option[];
    territoires: Option[];
    numerique: Option[];
    autres: Option[];
  }>(
    (acc, programme) => {
      const hasIndicateurs = definitions.some((definition) =>
        definition.tags.some(
          (tag) =>
            tag.name === "programme" && tag.value === programme.identifiant,
        ),
      );

      if (!hasIndicateurs) {
        return acc;
      }

      const category = CATEGORIES.find(
        (candidate) => candidate.identifiant === programme.categorie,
      ) || { identifiant: "autres" as const };

      return {
        ...acc,
        [category.identifiant]: [
          ...acc[category.identifiant],
          { value: programme.identifiant, label: programme.nom },
        ],
      };
    },
    {
      ville: [],
      territoires: [],
      numerique: [],
      aos: [],
      autres: [],
    },
  );
}

export function getProgrammesOptions(
  programmes: Programme[],
  definitions: Metadata[],
) {
  const programmesByCategorie = sortByCategorie(programmes, definitions);

  return CATEGORIES.map((category) => ({
    label: category.nom,
    value: category.nom,
    options: programmesByCategorie[category.identifiant],
  }));
}
