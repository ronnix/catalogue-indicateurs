import { PAYS_OPTION } from "@/constants/mailles";
import { TerritoireOption, TerritoiresOptions } from "@/types/select";

const GEO_API_URL = "https://geo.api.gouv.fr";

const REGIONS_OUTRE_MER = [
  "01", // Guadeloupe
  "02", // Martinique
  "03", // Guyane
  "04", // La Réunion
  "06", // Mayotte
];

const DOM_CODE = "999";

const REGIONS_SLUG = "regions";
const DEPARTEMENTS_SLUG = "departements";

type Territoire = { code: string; nom: string; maille: string };

type Departement = Territoire & { codeRegion: string };

export async function fetchTerritoires(): Promise<TerritoiresOptions> {
  const responses = await Promise.all(
    [DEPARTEMENTS_SLUG, REGIONS_SLUG].map((slug) =>
      fetch(`${GEO_API_URL}/${slug}`, {
        headers: {
          "Content-Type": "application/json",
        },
        cache: "force-cache",
      }),
    ),
  );

  const [departementsData, regionsData] = (await Promise.all(
    responses.map((response) => response.json()),
  )) as [Departement[], Territoire[]];

  const departementsMap = new Map<string, TerritoireOption[]>();

  for (const departement of departementsData) {
    const code =
      departement.code.length >= 3 ? DOM_CODE : departement.codeRegion;

    const current = departementsMap.get(code) || [];

    departementsMap.set(code, [
      ...current,
      {
        value: departement.code,
        label: departement.nom,
        maille: "departement",
      },
    ]);
  }

  const territoires = [...regionsData, { code: DOM_CODE, nom: "DOM" }]
    .filter((region) => !REGIONS_OUTRE_MER.includes(region.code))
    .map((region) => ({
      value: region.code,
      label: region.nom,
      maille: "region" as const,
      options: (departementsMap.get(region.code) || []).sort((a, b) =>
        a.label > b.label ? 1 : -1,
      ),
      selectable: region.code !== DOM_CODE,
    }))
    .sort((a, b) => {
      if (!a.selectable) {
        return 1;
      }
      return a.label > b.label ? 1 : -1;
    });

  return [PAYS_OPTION, ...territoires];
}
