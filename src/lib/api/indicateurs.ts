import * as Sentry from "@sentry/nextjs";
import { revalidateTag } from "next/cache";

import {
  DEPRECATED_METRICS,
  PROGRAMME_TAG_NAME,
} from "@/constants/indicateurs";
import { DefinitionIndicateur } from "@/types/indicateur";

import { fetchProgrammes } from "./programmes";
import logger from "../logger";

const INDICATEURS_API_URL = process.env.NEXT_PUBLIC_INDICATEURS_API_URL || "";

const DEFINITIONS_FETCH_REVALIDATE_DELAY = 3600;

const GET_DEFINITIONS_QUERY = `query getDefinitions {
  indicateurs {
    metadata {
      identifiant
      nom
      description
      categorie
      schema
      mailles
      tags {
        name
        value
      }
      unite
      recette
      datasets {
        name
        description
        table
        maintainer
        lastModified
        lastImport
        updateFrequency
        needsUpdate
        nextExpectedUpdate
        producer {
          name
          url
        }
        source
        pathPattern
        fileFormat
      }
    }
    }
}`;

export async function fetchDefinitions() {
  const response = await fetch(INDICATEURS_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({
      query: GET_DEFINITIONS_QUERY,
    }),
    next: {
      revalidate: DEFINITIONS_FETCH_REVALIDATE_DELAY,
      tags: ["definitions"],
    },
  });

  const programmes = await fetchProgrammes();

  const indicateurs: {
    data: {
      indicateurs: DefinitionIndicateur[];
    } | null;
    error?: unknown;
  } = await response.json();

  if (!indicateurs.data) {
    logger.error({
      message:
        "Failed to fetch definitions, trying to refetch on next request.",
      error: indicateurs.error,
    });

    Sentry.withScope((scope) => {
      scope.setExtra("cause", "Failed to fetch definitions.");
      Sentry.captureException(indicateurs.error);
    });

    revalidateTag("definitions");

    return [];
  }

  return indicateurs.data.indicateurs
    .filter((indicateur) => {
      if (DEPRECATED_METRICS.includes(indicateur.metadata.identifiant)) {
        return false;
      }

      if (!indicateur.metadata.schema) {
        logger.warn({
          message: "Indicateur without 'schema', ignoring",
          id: indicateur.metadata.identifiant,
        });

        return false;
      }

      if (!indicateur.metadata.nom) {
        logger.warn({
          message: "Indicateur without 'nom', ignoring",
          id: indicateur.metadata.identifiant,
        });

        return false;
      }

      return true;
    })
    .map((definition) => ({
      ...definition.metadata,
      tags: definition.metadata.tags
        .filter((tag) => tag.name === PROGRAMME_TAG_NAME)
        .map((tag) => {
          const match = programmes.find(
            (programme) => programme.identifiant === tag.value,
          );

          return { ...tag, nom: match?.nom };
        }),
    }));
}
