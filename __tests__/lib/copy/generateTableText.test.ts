import { generateTableText } from "@/lib/copy/generateTableText";

describe("generateTableText", () => {
  it("should return tablulated data", () => {
    const columns = [
      { key: "first", title: "First" },
      { key: "second", title: "Second" },
    ];

    const lines = [
      [123, 456],
      [987, 654],
    ];

    const result = generateTableText(columns, lines, "Label");

    expect(result).toEqual("Label\n\nFirst\tSecond\n123\t456\n987\t654");
  });

  it("should handle missing label", () => {
    const columns = [
      { key: "first", title: "First" },
      { key: "second", title: "Second" },
    ];

    const lines = [
      [123, 456],
      [987, 654],
    ];

    const result = generateTableText(columns, lines);

    expect(result).toEqual("First\tSecond\n123\t456\n987\t654");
  });
});
