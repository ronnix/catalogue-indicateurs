import { formatDate } from "@/lib/formatDate";

describe("formatDate", () => {
  it("should properly format dates", () => {
    const date = new Date(2023, 4, 10).toISOString();
    expect(formatDate(date)).toEqual("10/05/2023");
  });

  it("should handle null values)", () => {
    expect(formatDate(null)).toEqual("-");
  });

  it("should handle undefined values", () => {
    expect(formatDate(undefined)).toEqual("-");
  });
});
