import { removeEmpty } from "@/lib/removeEmpty";

describe("removeEmpty", () => {
  it("should return an empty array for a falsy input", () => {
    expect(removeEmpty(undefined)).toEqual([]);
    expect(removeEmpty(null)).toEqual([]);
  });

  it("should remove null and undefined values", () => {
    const input = [null, undefined, 123, "Bonjour"];

    expect(removeEmpty(input)).toEqual([123, "Bonjour"]);
  });

  it("should remove empty strings", () => {
    expect(removeEmpty([""])).toEqual([]);
  });
});
