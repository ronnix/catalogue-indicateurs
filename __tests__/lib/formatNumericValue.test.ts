import { formatNumericValue } from "@/lib/formatNumericValue";

describe("formatNumericValue", () => {
  it("should format a null input", () => {
    expect(formatNumericValue({ value: null })).toEqual("–");
  });

  it("should format values in euro (small amount)", () => {
    expect(formatNumericValue({ value: 12, unit: "€" })).toEqual("12\u00A0€");
  });

  it("should format values in euro (millions)", () => {
    expect(formatNumericValue({ value: 7840119.1234, unit: "€" })).toEqual(
      "7,8\u00A0M\u00A0€",
    );
  });

  it("should format integer values", () => {
    expect(formatNumericValue({ value: 12345 })).toEqual("12 345");
  });

  it("should format decimal values without units", () => {
    expect(formatNumericValue({ value: 0.544567 })).toEqual("54,5\u00A0%");
  });

  it("should accept a custom multiplier for decimal values without units", () => {
    expect(formatNumericValue({ value: 58.12, multiplier: 1 })).toEqual(
      "58,1\u00A0%",
    );
  });
});
