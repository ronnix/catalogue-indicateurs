import { renderUnit } from "@/lib/renderUnit";

describe("renderUnit", () => {
  it("should handle percent unit", () => {
    const result = renderUnit("percent");

    expect(result).toEqual("%");
  });

  it("should handle regular units", () => {
    const result = renderUnit("€");

    expect(result).toEqual(" €");
  });
});
