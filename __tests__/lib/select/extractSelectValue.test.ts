import { extractSelectValue } from "@/lib/select/extractSelectValue";
import { TerritoireOption } from "@/types/select";

describe("extractSelectValue", () => {
  it("should extract matching select value", () => {
    const options = [
      {
        label: "Île-de-France",
        value: "11",
        maille: "region" as const,
        options: [
          { label: "Paris", value: "75", maille: "departement" as const },
        ],
      },
      {
        label: "Occitanie",
        value: "76",
        maille: "region" as const,
        options: [
          { label: "Aude", value: "11", maille: "departement" as const },
          {
            label: "Haute-Garonne",
            value: "31",
            maille: "departement" as const,
          },
        ],
      },
    ];

    const matcher = (option: TerritoireOption) =>
      option.value === "31" && option.maille === "departement";

    const result = extractSelectValue<TerritoireOption>(options, matcher);

    expect(result).toEqual({
      label: "Haute-Garonne",
      value: "31",
      maille: "departement",
    });
  });
});
