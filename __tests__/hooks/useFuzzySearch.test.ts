import { renderHook } from "@testing-library/react";

import { FUSE_OPTIONS } from "@/constants/search";
import { useFuzzySearch } from "@/hooks/useFuzzySearch";

import indicateurs from "../__fixtures__/indicateurs";

describe("useFuzzySearch", () => {
  it("should return matching results ranked by score", () => {
    const { result } = renderHook(() =>
      useFuzzySearch("cret", indicateurs.data.indicateurs, {
        ...FUSE_OPTIONS,
        threshold: 0.3,
      }),
    );

    const ids = result.current.map((item) => item.identifiant);

    expect(ids).toEqual([
      "crte",
      "nb_crte",
      "noms_crte",
      "credits_p147_2022",
      "credits_cites_educatives_2022",
      "credits_cites_emploi_2022",
    ]);
  });
});
