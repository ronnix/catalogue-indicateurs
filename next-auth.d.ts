import NextAuth from "next-auth";

declare module "next-auth" {
  interface Profile {
    email: string;
    email_verified: boolean;
    family_name: string;
    given_name: string;
    updated_at: string;
    job: string;
    organizations: {
      id: number;
      siret: string;
      is_external: boolean;
      label: string | null;
      is_collectivite_territoriale: boolean;
      is_service_public: boolean;
    }[];
  }

  interface Session extends DefaultSession {
    profile?: Profile;

    accessToken?: string;
  }
}

declare module "next-auth/jwt" {
  interface JWT {
    accessToken?: string;
    profile?: Profile;
  }
}
