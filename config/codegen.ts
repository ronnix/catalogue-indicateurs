import { CodegenConfig } from "@graphql-codegen/cli";
import { loadEnvConfig } from "@next/env";

loadEnvConfig(process.cwd());

const config: CodegenConfig = {
  schema: process.env.NEXT_PUBLIC_INDICATEURS_API_URL,
  generates: {
    "./src/types/schema.ts": {
      plugins: [
        "typescript",
        {
          add: {
            content: "/* eslint-disable */",
          },
        },
      ],
    },
  },
  hooks: { afterAllFileWrite: ["prettier --write"] },
};

export default config;
