# Catalogue d’indicateurs (metrics catalog)

This project is a web application aiming to be a showcase of metrics (indicateurs) for all programs of the ANCT ([Agence Nationale de la Cohésion des Territoires](https://agence-cohesion-territoires.gouv.fr/)), gathered, aggregated and exposed by our inSITu project.

The main purpose is to help public agents easily access and use our aggregated data, but the project is public and can therefore be accessed by anyone.

## Accessing the application

The application can be accessed here: https://catalogue-indicateurs.donnees.incubateur.anct.gouv.fr/.

Note that it is still in the early development stages.

## Local setup

This project is based on [Next.js](https://nextjs.org/), which means you will need [Node.js](https://nodejs.org/fr) installed on your system.

Once done, you will need to install dependencies by running:
```bash
npm install
```

Create a `.env.local` file at the root using the `.env.local.example` sample file

You should then be able to start the project with:
```bash
npm run dev
```

And the project will be accessible on `http://localhost:3000`.

## Updating the list of programs / categories

Metrics are grouped by categories which we call "programs". This concept of programs is not known to the inSITu project (which merely defines abstract tags on metrics) and is purely a catalog specific context.

For this reason, if you want to create a new category of metrics or change an existing one, you need to update the catalog configuration located at `src/constants/programmes.ts`.

## Updating the definitions of metrics

The list of metrics exposed by inSITu is cached in the catalog to limit redundant network requests.

If you update a metric or create a new one, you might need invalidate the definitions cache. To do so, you can call this route manually : https://catalogue-indicateurs.donnees.incubateur.anct.gouv.fr/api/revalidate?tag=definitions (it works from a browser, or from curl).

## Contributing

### Next 13 app router

This project uses Next 13's 'app' router, which although advertised as stable is still fairly new, and uses React Server Components.

You can get familiar with its principles by reading [the documentation](https://nextjs.org/docs)

### Git branches

The project has two main branches:
* `main`: The [production environment](https://catalogue-indicateurs.donnees.incubateur.anct.gouv.fr/) is based on this branch. When code is pushed on it, a deploy will occur in production.
* `develop`: The [dev environment](https://catalogue-indicateurs.donnees.dev.incubateur.anct.gouv.fr/) is based on this branch. When code is pushed on it, a deploy will occur on the dev environment.

When starting to work on a new feature, create a new branch from develop.

When your feature is ready, push your code on the remote and open a Merge Request, which will be automatically pointing at develop.

Once your MR has been approved and merged, you can test your feature on the dev environment.

Once you are happy with your tests and want to deploy your feature in production, you can create a MR from `develop` targetting `main` and merge it to deploy.

Note that not only your feature might be included in such a situation, so make sure to sync with other contributors before releasing their work in production.
