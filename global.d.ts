type DSFR = {
  colors: {
    getColor: (string, string, string) => string;
  };
};

declare global {
  // eslint-disable-next-line no-var
  var dsfr: DSFR;
}

export {};
